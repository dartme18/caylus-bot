DEBUG=true
ifeq "$(DEBUG)" "true"
DEBUGOPTS=-g -O0
else
DEBUGOPTS=-O3 -DNDEBUG
endif
CC=g++
CXXFLAGS=-Wall -Werror -Wextra -pedantic -std=c++17 $(DEBUGOPTS)
sources=$(wildcard src/*.cpp)
objects=$(sources:src/%.cpp=obj/%.o)
headers=$(wildcard src/*.hpp)
precompiledheader=src/stdafx.hpp.gch
executable=dist/caylus-bot
resourcesrc=$(shell find resources/ -type f)
resourcedest=$(resourcesrc:resources/%=dist/%)
headerdependencies=$(objects:obj/%.o=obj/%.d) $(test_objects:obj/%.o=obj/%.d)
gtest_dir=src/test/googletest/googletest
gtest_headers=$(gtest_dir)/include/gtest/*.h $(gtest_dir)/include/gtest/internal/*.h
gtest_sources=$(gtest_dir)/src/*.cc $(gtest_dir)/src/*.h $(gtest_headers)
tests=$(wildcard src/test/*.cpp)
test_objects=$(tests:src/test/%.cpp=obj/test/%.o)

# "|" signals an order-only prerequisite: the prereq must exist, but
# it won't cause the target to be recreated.
$(executable): $(precompiledheader) $(objects) Makefile | obj dist $(resourcedest)
	@echo Linking $@...
	@$(CC) $(CXXFLAGS) -o $@ $(objects) -lstdc++fs -lpthread
	@echo Linked $@

$(resourcedest) : dist/% : resources/% | dist
	mkdir -p $(dir $@);
	cp -r $< $@;

obj dist obj/test:
	if [[ ! -e $@ ]]; then mkdir -p $@; fi;

-include $(headerdependencies)

reformat:
	for file in $(sources) $(headers) $(tests); do \
		clang-format -i -style="$$(<style)" "$$file" & \
		while [[ "$$(jobs | wc -l)" -ge "$$(grep processor /proc/cpuinfo | wc -l)" ]]; do \
			sleep .005; \
		done \
	done;

# Static pattern rules. Take each object in the targets, create a "base"
# with it specified by % (removing the literal part '.o'), then
# use that base to generate the prerequisite. This generates
# several rules.  For instance,  main.o : "main".o : "main".cpp
# then refer to the left-most prerequisite with $<, and the target
# with $@ like usual.
$(objects) : obj/%.o : src/%.cpp $(precompiledheader) Makefile | obj
	@echo Making $@...
	@$(CC) -c $(CXXFLAGS) -MMD -isystem src/spdlog/include $< -o $@
	@echo Made $@

$(precompiledheader) : src/stdafx.hpp src/logging.hpp Makefile | obj
	@echo Making $@...
	@$(CC) $(CXXFLAGS) -Isrc/spdlog/include $< -o $@
	@echo Made $@

dist/test : $(test_objects) $(filter-out obj/main.o, $(objects)) obj/test/gtest_main.a Makefile | dist
	@echo Linking $@...
	@$(CC) $(CXXFLAGS) -pthread -isystem src/test/googletest/googletest/include \
		$(test_objects) $(filter-out obj/main.o, $(objects)) obj/test/gtest_main.a -o $@
	@echo linked $@

$(test_objects) : obj/test/%.o : src/test/%.cpp $(precompiledheader) Makefile | obj/test
	@echo Making $@...
	@$(CC) -c $(CXXFLAGS) -MMD -isystem src/spdlog/include -isystem $(gtest_dir)/include $< -o $@
	@echo Made $@

obj/test/gtest_main.a : obj/test/gtest-all.o obj/test/gtest_main.o Makefile | obj/test
	@echo Making $@...
	@$(AR) r $@ obj/test/gtest-all.o obj/test/gtest_main.o
	@echo Made $@

obj/test/gtest-all.o obj/test/gtest_main.o : obj/test/%.o : $(gtest_dir)/src/%.cc $(gtest_sources) Makefile | obj/test
	@echo Making $@...
	@$(CC) -isystem $(gtest_dir)/include -I$(gtest_dir) $(CXXFLAGS) -o $@ -c $<
	@echo Made $@

clean :
	rm -rf obj dist $(precompiledheader)

echo :
	@echo sources $(sources)
	@echo objects $(objects)
	@echo headers $(headers) src/stdafx.hpp
	@echo executable $(executable)
	@echo resourcesrc $(resourcesrc)
	@echo resourcedest $(resourcedest)

.PHONY : echo clean reformat
