#pragma once

using move = std::variant<struct no_move, struct place_worker, struct gate,
    struct provost_move, struct pick_good, struct castle_packet, struct favor_move, struct building_move>;

enum struct good
{
    food,
    wood,
    stone,
    cloth,
    gold,
    none
};

using packet = std::array<good, 2>;

enum struct building_name
{
    pink_wood,
    pink_stone,
    pink_cloth, // food or cloth
    pink_market,
    carpenter,
    pedlar,
    fixed_pedlar,
    pink_food, // food or wood
    gate,
    trading_post,
    merchant_guild,
    joust_field,
    stables0,
    stables1,
    stables2,
    bridge0, // bridges have to be buildings because
    bridge1, // buildings have owners
    bridge2, // if bridges are only spots, then spots
    bridge3, // have to have owners which doesn't seem so great.
    bridge4,
    inn0,
    inn1,
    castle0,
    castle1,
    castle2,
    castle3,
    castle4,
    mason,
    wood_food_farm, // two food or one cloth
    wood_cloth_farm, // two cloth or one food
    lumberjack, // two wood
    marketplace,
    lawyer,
    stone_farm, // two food and one cloth
    park, // stone two wood one food
    none,
};

// Must be in order of activation
enum struct spot_name
{
    gate,
    trading_post,
    merchant_guild,
    joust_field,
    stables0,
    stables1,
    stables2,
    inn0, // the resting inn-occupying spot
    inn1, // to take the inn
    bridge0,
    bridge1,
    bridge2,
    bridge3,
    bridge4,
    pink0,
    pink1,
    pink2,
    pink3,
    pink4,
    pink5,
    fixed_carpenter,
    fixed_pedlar,
    build0,
    build1,
    build2, // score dungeons
    build3,
    build4,
    build5,
    build6,
    fixed_gold,
    build7,
    build8, //score walls
    build9,
    build10,
    build11,
    build12,
    castle0,
    castle1,
    castle2,
    castle3,
    castle4,
    none,
};

enum struct building_type
{
    builtin,
    wood,
    stone,
    green,
    blue,
    none
};

enum struct favor_track
{
    prestige,
    money,
    goods,
    building
};

struct move_base
{
    move_base(unsigned player);
    virtual std::string to_string(void) const;
    unsigned player;
    virtual ~move_base(void) { }
};

struct place_worker : public move_base
{
    place_worker(unsigned player, spot_name spot);
    spot_name spot;
    std::string to_string(void) const override;
};

struct gate : public move_base
{
    gate(unsigned player, spot_name destination);
    spot_name destination;
    std::string to_string(void) const override;
};

struct provost_move : public move_base
{
    provost_move(unsigned player, int provost_move);
    int move_count;
    std::string to_string(void) const override;
};

struct no_move : public move_base
{
    no_move(void);
};

struct pick_good : public move_base
{
    pick_good(unsigned player, good picked);
    good picked;
    std::string to_string(void) const override;
};

struct castle_packet : public move_base
{
    castle_packet(unsigned player, std::vector<packet> && picked);
    std::vector<packet> picked;
    std::string to_string(void) const override;
};

struct favor_move : public move_base
{
    favor_move(unsigned player, favor_track, unsigned column);
    favor_move(unsigned player, good receiving);
    favor_move(unsigned player, good giving, good receiving0, good receiving1);
    favor_move(unsigned player, building_name);
    favor_track track;
    // This refers to which spot on the favor track to exercise
    // For prestige and money, the last possible spot will be
    // exercised. For goods and buildings, the choice is yours.
    // For goods, these don't correspond to the favor columns on the board:
    // 1 means food, 2 means wood, 3 means stone, 4 means cloth,
    //  5 means trade, 6 means gold.
    unsigned column;
    // If you choose the food track, this is at least one of the
    // goods you want to receive
    good receiving0 {good::none};
    // If you choose spot 4 on the goods track (trade),
    // specify what you're giving as part of the trade.
    good giving {good::none};
    good receiving1 {good::none};
    building_name building {building_name::none};
    std::string to_string(void) const override;
};

struct building_move : public move_base
{
    building_move(unsigned player, building_name to_build);
    building_name to_build;
    std::string to_string(void) const override;
};

struct building
{
    building(building_name);
    std::string to_string(void) const;
    building_name name;
    int owner {-1};
    // How many deniers are generated at income phase
    unsigned generated_deniers {0};
    int occupied {-1};
};

std::vector<building> const & get_buildings(void);
building const & get_building(building_name);

bool operator==(building const & lhs, building const & rhs);
bool operator!=(building const & lhs, building const & rhs);
bool operator<(building const & lhs, building const & rhs);

std::string to_string(spot_name);
std::string to_string(move const &);
std::string to_string(building_name);
std::string to_string(building_type);
std::string to_string(good);
std::string to_string(favor_track);

bool is_pink(spot_name);
bool is_pink(building_name);
bool is_pink(building const &);
int get_random_number(int min, int max);
std::vector<building_name> get_pink_buildings(void);
bool can_place(building_name);
bool is_bridge(spot_name);
bool is_bridge(building_name);
bool is_fixed(building_name);
// Returns how many of this type of building is allowed to be on the board.
unsigned allowed_building_count(building_name);

// Transforms 0 to spot_name::bridge0, etc.
// Numbers that don't correspond to bridge spots convert to spot_name::none
spot_name get_bridge_spot(unsigned);
unsigned get_bridge_spot(spot_name);
// If require is specified, an exception is thrown if the building cannot be found
std::vector<std::pair<spot_name, building>>::iterator find_building(std::vector<std::pair<spot_name, building>> &, spot_name, bool require = false);
std::vector<std::pair<spot_name, building>>::const_iterator find_building(std::vector<std::pair<spot_name, building>> const &, spot_name, bool require = false);

struct spot_name_iterator
{
    spot_name_iterator(void);
    // prefix
    spot_name_iterator operator++(void);
    spot_name operator*(void);
    static spot_name_iterator begin(void);
    static spot_name_iterator end(void);

private:
    spot_name val;
};

// Iterates the building_names in no particular order
struct building_iterator
{
    building_iterator(void);
    building_iterator(building_name);
    // pre-increment
    building_iterator & operator++(void);
    building_name operator*(void);
    static building_iterator begin(void);
    static building_iterator end(void);

private:
    building_name val;
};

struct good_iterator
{
    good_iterator(void);
    good_iterator operator++(void);
    good operator*(void);
    static good_iterator begin(void);
    static good_iterator end(void);

private:
    good val;
};

bool operator!=(building_iterator, building_iterator);
// prefix
spot_name operator++(spot_name &);
spot_name operator--(spot_name &);
// prefix
good operator++(good &);
building_type get_building_type(building_name);
bool contains(std::vector<std::pair<spot_name, building>> const &, building_name);
unsigned get_player(move const &);
// Returns how many prestige are gained by building a particular building
unsigned prestige_from_build(building_name);
// Given a list of non-food goods, returns all possible castle packets omitting the empty packet
// Assumes one food is avaible to finish the packet
std::vector<packet> get_all_unique_packets(std::vector<good> & non_food_goods);
std::vector<std::vector<packet>> get_possible_packets(unsigned food, std::vector<good> && other_goods);
bool operator==(packet const &, packet const &);
bool operator==(std::vector<std::vector<good>>, std::vector<std::vector<good>>);
