#include "stdafx.hpp"

#include "util.hpp"

std::string move_base::to_string(void) const
{
    return "Player " + std::to_string(player);
}

bool is_pink(spot_name name)
{
    return name == spot_name::pink0 || name == spot_name::pink1 || name == spot_name::pink2 || name == spot_name::pink3 || name == spot_name::pink4 || name == spot_name::pink5;
}

std::string to_string(spot_name n)
{
    switch (n)
    {
    case spot_name::none: return "none";
    case spot_name::gate: return "gate";
    case spot_name::trading_post: return "trading_post";
    case spot_name::merchant_guild: return "merchant_guild";
    case spot_name::joust_field: return "joust_field";
    case spot_name::stables0: return "stables0";
    case spot_name::stables1: return "stables1";
    case spot_name::stables2: return "stables2";
    case spot_name::inn0: return "inn0";
    case spot_name::inn1: return "inn1";
    case spot_name::pink0: return "pink0";
    case spot_name::pink1: return "pink1";
    case spot_name::pink2: return "pink2";
    case spot_name::pink3: return "pink3";
    case spot_name::pink4: return "pink4";
    case spot_name::pink5: return "pink5";
    case spot_name::bridge0: return "bridge0";
    case spot_name::bridge1: return "bridge1";
    case spot_name::bridge2: return "bridge2";
    case spot_name::bridge3: return "bridge3";
    case spot_name::bridge4: return "bridge4";
    case spot_name::build0: return "build0";
    case spot_name::build1: return "build1";
    case spot_name::build2: return "build2";
    case spot_name::build3: return "build3";
    case spot_name::build4: return "build4";
    case spot_name::build5: return "build5";
    case spot_name::fixed_carpenter: return "fixed_carpenter";
    case spot_name::fixed_pedlar: return "fixed_pedlar";
    case spot_name::build6: return "build6";
    case spot_name::build7: return "build7";
    case spot_name::build8: return "build8";
    case spot_name::fixed_gold: return "fixed_gold";
    case spot_name::build9: return "build9";
    case spot_name::build10: return "build10";
    case spot_name::build11: return "build11";
    case spot_name::build12: return "build12";
    case spot_name::castle0: return "castle0";
    case spot_name::castle1: return "castle1";
    case spot_name::castle2: return "castle2";
    case spot_name::castle3: return "castle3";
    case spot_name::castle4: return "castle4";
    }
    return "ERROR_MISSING";
}

std::string to_string(building_name n)
{
    switch (n)
    {
    case building_name::none: return "none";
    case building_name::bridge0: return "bridge0";
    case building_name::bridge1: return "bridge1";
    case building_name::bridge2: return "bridge2";
    case building_name::bridge3: return "bridge3";
    case building_name::bridge4: return "bridge4";
    case building_name::pink_wood: return "pink_wood";
    case building_name::pink_stone: return "pink_stone";
    case building_name::pink_cloth: return "pink_cloth";
    case building_name::pink_market: return "pink_market";
    case building_name::carpenter: return "carpenter";
    case building_name::pink_food: return "pink_food";
    case building_name::gate: return "gate";
    case building_name::trading_post: return "trading_post";
    case building_name::merchant_guild: return "merchant_guild";
    case building_name::joust_field: return "joust_field";
    case building_name::stables0: return "stables0";
    case building_name::stables1: return "stables1";
    case building_name::stables2: return "stables2";
    case building_name::inn0: return "inn0";
    case building_name::inn1: return "inn1";
    case building_name::castle0: return "castle0";
    case building_name::castle1: return "castle1";
    case building_name::castle2: return "castle2";
    case building_name::castle3: return "castle3";
    case building_name::castle4: return "castle4";
    case building_name::mason: return "mason";
    case building_name::pedlar: return "pedlar";
    case building_name::fixed_pedlar: return "fixed_pedlar";
    case building_name::wood_food_farm: return "wood_food_farm";
    case building_name::wood_cloth_farm: return "wood_cloth_farm";
    case building_name::marketplace: return "marketplace";
    case building_name::lawyer: return "lawyer";
    case building_name::lumberjack: return "lumberjack";
    case building_name::stone_farm: return "stone_farm";
    case building_name::park: return "park";
    }
    return "MISSING BUILDING NAME";
}

building::building(building_name n)
    : name {n}
{
}

std::string building::to_string(void) const
{
    return ::to_string(name);
}

std::mt19937 gen {std::random_device {}()};
int get_random_number(int min, int max)
{
    auto dist {std::uniform_int_distribution {min, max}};
    return dist(gen);
}

std::string to_string(move const & m)
{
    return std::visit([](auto const & m)
        { return m.to_string(); },
        m);
}

place_worker::place_worker(unsigned player, spot_name spot)
    : move_base {player}
    , spot {spot}
{
}

std::string place_worker::to_string(void) const
{
    return "Place worker move; player " + std::to_string(player) + " spot " + ::to_string(spot);
}

gate::gate(unsigned player, spot_name d)
    : move_base {player}
    , destination {d}
{
}

std::string gate::to_string(void) const
{
    return "gate -> " + ::to_string(destination);
}

favor_move::favor_move(unsigned player, favor_track f, unsigned column)
    : move_base {player}
    , track {f}
    , column {column}
{
    if (f == favor_track::money || f == favor_track::prestige)
        column = 0; // ignored
    else
        throw std::invalid_argument {"Favors must specify a column unless they are deniers or prestige."};
}

favor_move::favor_move(unsigned player, good receiving)
    : move_base {player}
    , track {favor_track::goods}
    , receiving0 {receiving}
{
    switch (receiving)
    {
    case good::food: column = 1; break;
    case good::wood: column = 2; break;
    case good::stone: column = 3; break;
    case good::cloth: column = 4; break;
    case good::gold: column = 6; break;
    default: throw std::invalid_argument {"Invalid receiving good for favor move."};
    }
}

favor_move::favor_move(unsigned player, good giving, good rec0, good rec1)
    : move_base {player}
    , track {favor_track::goods}
    , column {4}
    , receiving0 {rec0}
    , giving {giving}
    , receiving1 {rec1}
{
}

favor_move::favor_move(unsigned player, building_name b)
    : move_base {player}
    , track {favor_track::building}
    , building {b}
{
    switch (get_building_type(b))
    {
    case building_type::none: column = 1; break;
    case building_type::wood: column = 2; break;
    case building_type::stone: column = 3; break;
    case building_type::green: column = 4; break;
    case building_type::blue: column = 5; break;
    default: throw std::invalid_argument {"Invalid building type for favor: " + ::to_string(b)};
    }
}

std::string favor_move::to_string(void) const
{
    return "Choose favor track: " + ::to_string(track);
}

provost_move::provost_move(unsigned player, int m)
    : move_base {player}
    , move_count {m}
{
}

std::string provost_move::to_string(void) const
{
    return "provost_move:" + std::to_string(move_count);
}

no_move::no_move(void)
    : move_base {255}
{
}

move_base::move_base(unsigned p)
    : player {p}
{
}

std::vector<building_name> get_pink_buildings(void)
{
    return {building_name::pink_wood, building_name::pink_stone, building_name::pink_cloth,
        building_name::pink_market, building_name::carpenter, building_name::pink_food};
}

pick_good::pick_good(unsigned player, good picked)
    : move_base {player}
    , picked {picked}
{
}

std::string pick_good::to_string(void) const
{
    return "Pick " + ::to_string(picked);
}

castle_packet::castle_packet(unsigned player, std::vector<packet> && goods)
    : move_base {player}
    , picked {std::move(goods)}
{
}

std::string castle_packet::to_string(void) const
{
    return std::to_string(picked.size()) + " packets";
}

bool can_place(building_name n)
{
    switch (n)
    {
    case building_name::none:
        return false;
    default:
        return true;
    }
}

bool is_bridge(spot_name n)
{
    switch (n)
    {
    case spot_name::bridge0:
    case spot_name::bridge1:
    case spot_name::bridge2:
    case spot_name::bridge3:
    case spot_name::bridge4: return true;
    default: return false;
    }
}

spot_name get_bridge_spot(unsigned num)
{
    switch (num)
    {
    case 0: return spot_name::bridge0;
    case 1: return spot_name::bridge1;
    case 2: return spot_name::bridge2;
    case 3: return spot_name::bridge3;
    case 4: return spot_name::bridge4;
    default: return spot_name::none;
    }
}

unsigned get_bridge_spot(spot_name n)
{
    switch (n)
    {
    case spot_name::bridge0: return 0;
    case spot_name::bridge1: return 1;
    case spot_name::bridge2: return 2;
    case spot_name::bridge3: return 3;
    case spot_name::bridge4: return 4;
    default: throw std::runtime_error {"Not a bridge spot: " + ::to_string(n)};
    }
}

std::vector<std::pair<spot_name, building>>::iterator find_building(
    std::vector<std::pair<spot_name, building>> & buildings, spot_name n, bool require)
{
    auto ret {find_building(const_cast<std::vector<std::pair<spot_name, building>> const &>(buildings), n, require)};
    // Dirty, dirty trick to get an iterator from a const_iterator
    return buildings.erase(ret, ret);
}

std::vector<std::pair<spot_name, building>>::const_iterator find_building(
    std::vector<std::pair<spot_name, building>> const & buildings, spot_name n, bool require)
{
    auto building_spot {std::find_if(buildings.begin(), buildings.end(), [n](auto const & s)
        { return s.first == n; })};
    if (require && building_spot == buildings.end())
        throw std::runtime_error {"Spot " + to_string(n) + " not found."};
    return building_spot;
}

building const & get_building(building_name n)
{
    auto const & bs {get_buildings()};
    auto spot {std::find_if(bs.begin(), bs.end(), [n](building const & b)
        { return b.name == n; })};
    if (spot == bs.end())
        throw std::runtime_error {"Couldn't find building \"" + ::to_string(n) + "\" in buildings."};
    return *spot;
}

bool is_bridge(building_name n)
{
    switch (n)
    {
    case building_name::bridge0:
    case building_name::bridge1:
    case building_name::bridge2:
    case building_name::bridge3:
    case building_name::bridge4: return true;
    default: return false;
    }
}

std::vector<building> get_buildings_impl(void)
{
    std::vector<building> ret;
    std::transform(building_iterator::begin(), building_iterator::end(), std::back_inserter(ret), [](building_name n)
        { return building {n}; });
    return ret;
}

std::vector<building> const & get_buildings(void)
{
    static std::vector<building> ret {get_buildings_impl()};
    return ret;
}

building_iterator::building_iterator()
    : val {building_name::pink_wood}
{
}

building_iterator building_iterator::begin(void)
{
    return building_iterator {};
}

building_name building_iterator::operator*(void)
{
    return val;
}

building_iterator & building_iterator::operator++(void)
{
    if (val == building_name::none)
        return val = building_name::pink_wood, *this;
    return val = static_cast<building_name>(static_cast<int>(val) + 1), *this;
}

building_iterator building_iterator::end(void)
{
    return building_iterator {building_name::none};
}

building_iterator::building_iterator(building_name n)
    : val {n}
{
}

bool operator!=(building_iterator lhs, building_iterator rhs)
{
    return *lhs != *rhs;
}

std::string to_string(good g)
{
    switch (g)
    {
    case good::food: return "food";
    case good::stone: return "stone";
    case good::wood: return "wood";
    case good::cloth: return "cloth";
    case good::gold: return "gold";
    case good::none: return "no good";
    }
    throw std::runtime_error {"Couldn't find good?"};
}

std::string to_string(favor_track f)
{
    switch (f)
    {
    case favor_track::prestige: return "prestige";
    case favor_track::money: return "money";
    case favor_track::goods: return "goods";
    case favor_track::building: return "building";
    }
    throw std::runtime_error {"Couldn't find that favor track"};
}

spot_name operator--(spot_name & s)
{
    if (s == spot_name::gate)
        return spot_name::none;
    return s = static_cast<spot_name>(static_cast<int>(s) - 1);
}

spot_name operator++(spot_name & s)
{
    if (s == spot_name::none)
        return spot_name::gate;
    return s = static_cast<spot_name>(static_cast<int>(s) + 1);
}

std::string to_string(building_type t)
{
    switch (t)
    {
    case building_type::builtin: return "builtin";
    case building_type::wood: return "wood";
    case building_type::stone: return "stone";
    case building_type::green: return "green";
    case building_type::blue: return "blue";
    case building_type::none: return "none";
    }
    throw std::runtime_error {"Couldn't find building type"};
}

building_type get_building_type(building_name n)
{
    switch (n)
    {
    case building_name::pink_wood:
    case building_name::pink_stone:
    case building_name::pink_cloth:
    case building_name::pink_market:
    case building_name::fixed_pedlar:
    case building_name::carpenter:
    case building_name::pink_food:
    case building_name::gate:
    case building_name::trading_post:
    case building_name::merchant_guild:
    case building_name::joust_field:
    case building_name::stables0:
    case building_name::stables1:
    case building_name::stables2:
    case building_name::bridge0:
    case building_name::bridge1:
    case building_name::bridge2:
    case building_name::bridge3:
    case building_name::bridge4:
    case building_name::inn0:
    case building_name::inn1:
    case building_name::castle0:
    case building_name::castle1:
    case building_name::castle2:
    case building_name::castle3:
    case building_name::castle4: return building_type::builtin;
    case building_name::pedlar:
    case building_name::wood_food_farm:
    case building_name::wood_cloth_farm:
    case building_name::marketplace:
    case building_name::lawyer:
    case building_name::lumberjack:
    case building_name::mason: return building_type::wood;
    case building_name::stone_farm:
    case building_name::park:
    case building_name::none: return building_type::none;
    }
    throw std::runtime_error {"Couldn't find building name for building_type"};
}

good operator++(good & g)
{
    if (g == good::none)
        return g = good::wood;
    return g = static_cast<good>(static_cast<int>(g) + 1);
}

building_move::building_move(unsigned player, building_name to_build)
    : move_base {player}
    , to_build {to_build}
{
}

std::string building_move::to_string(void) const
{
    return "Building move building " + ::to_string(to_build);
}

bool contains(std::vector<std::pair<spot_name, building>> const & buildings, building_name b)
{
    return std::find_if(buildings.begin(), buildings.end(), [b](auto const & building)
               { return building.second.name == b; })
        != buildings.end();
}

unsigned get_player(move const & m)
{
    return std::visit([](auto const & m)
        { return m.player; },
        m);
}

unsigned allowed_building_count(building_name b)
{
    switch (b)
    {
    case building_name::pedlar: return 2;
    default: return 1;
    }
}

unsigned prestige_from_build(building_name n)
{
    switch (n)
    {
    case building_name::lumberjack:
    case building_name::wood_food_farm:
    case building_name::wood_cloth_farm: return 2;
    case building_name::pedlar:
    case building_name::lawyer:
    case building_name::marketplace:
    case building_name::mason: return 4;
    default: return 0;
    }
}

using packets = std::vector<packet>;

packets get_all_unique_packets(std::vector<good> & non_food_goods)
{
    if (non_food_goods.size() < 2)
        return {};
    packets ret;
    std::sort(non_food_goods.begin(), non_food_goods.end());
    for (unsigned i {0}; i < non_food_goods.size() - 1; ++i)
    {
        if (i && non_food_goods[i] == non_food_goods[i - 1])
            continue;
        for (unsigned j {i + 1}; j < non_food_goods.size(); ++j)
        {
            if (non_food_goods[j] == non_food_goods[i])
                continue;
            if (j && non_food_goods[j - 1] == non_food_goods[j])
                continue;
            // We should have a set of unique goods that hasn't already been picked.
            ret.push_back({non_food_goods[i], non_food_goods[j]});
        }
    }
    return ret;
}

std::string to_string(std::vector<good> const & g)
{
    std::ostringstream str;
    str << '{';
    for (good temp : g)
        str << to_string(temp) << ',';
    str << '}';
    return str.str();
}

std::string to_string(packet const & g)
{
    std::ostringstream str;
    str << '{';
    for (good temp : g)
        str << to_string(temp) << ',';
    str << '}';
    return str.str();
}

std::string to_string(packets const & p)
{
    std::ostringstream str;
    str << '{';
    for (packet const & temp : p)
        str << to_string(temp) << ",";
    str << '}';
    return str.str();
}

std::string to_string(std::vector<packets> const & p)
{
    std::ostringstream str;
    str << '{';
    for (packets const & temp : p)
        str << to_string(temp) << ',';
    str << '}';
    return str.str();
}

std::vector<good> group_goods(packets const & p)
{
    std::vector<good> ret;
    std::for_each(p.begin(), p.end(), [&ret](packet const & temp)
        {
            ret.push_back(temp[0]);
            ret.push_back(temp[1]);
        });
    return ret;
}

bool operator==(packet const & lhs, packet const & rhs)
{
    return ((lhs.at(0) == rhs.at(0) && lhs.at(1) == rhs.at(1)) || (lhs.at(1) == rhs.at(0) && lhs.at(0) == rhs.at(1)));
}

bool operator==(packets lhs, packets rhs)
{
    if (lhs.size() != rhs.size())
        return false;
    auto lhs_vec {group_goods(lhs)};
    auto rhs_vec {group_goods(rhs)};
    std::sort(lhs_vec.begin(), lhs_vec.end());
    std::sort(rhs_vec.begin(), rhs_vec.end());
    for (unsigned i {0}; i < lhs_vec.size(); ++i)
        if (!(lhs_vec[i] == rhs_vec[i]))
            return false;
    return true;
}

bool operator<(packet const & lhs, packet const & rhs)
{
    if (lhs[0] < rhs[0])
        return true;
    else if (rhs[0] < lhs[0])
        return false;
    if (lhs[1] < rhs[1])
        return true;
    return rhs[1] < lhs[1];
}

bool operator<(packets const & lhs, packets const & rhs)
{
    if (lhs.size() == 0 && rhs.size() == 0)
        // They're equal
        return false;
    if (lhs.size() != rhs.size())
    {
        if (lhs.size() < rhs.size())
            return true;
        return false;
    }
    auto lhs_vec {group_goods(lhs)};
    auto rhs_vec {group_goods(rhs)};
    std::sort(lhs_vec.begin(), lhs_vec.end());
    std::sort(rhs_vec.begin(), rhs_vec.end());
    for (unsigned i {0}; i < lhs_vec.size(); ++i)
    {
        if (lhs_vec[i] < rhs_vec[i])
            return true;
        if (rhs_vec[i] < lhs_vec[i])
            return false;
    }
    // They're equal
    return false;
}

/*
 * food, food, food, wood, wood, cloth, cloth, stone, gold
 *
 * ;
 * wood, stone;
 * wood, cloth;
 * wood, gold;
 * stone, cloth;
 * stone, gold;
 * cloth, gold;
 * wood, stone; wood, cloth;
 * wood, stone; wood, gold;
 * wood, cloth; wood, cloth;
 * wood, cloth; wood, gold;
 * wood, cloth; stone, cloth;
 * wood, gold; stone, cloth;
 * wood, cloth; cloth, gold;
 * stone, cloth; cloth, gold;
 * wood, cloth; wood, gold; stone, cloth;
 */
std::vector<packets> get_possible_packets_impl(unsigned food, std::vector<good> && other_goods)
{
    std::vector<packets> ret;
    ret.push_back(packets {});
    packets starters {get_all_unique_packets(other_goods)};
    for (packet const & p : starters)
    {
        std::vector<good> copy {other_goods};
        for (good g : p)
            copy.erase(std::find(copy.begin(), copy.end(), g));
        std::vector<packets> next_step {get_possible_packets_impl(food - 1, std::move(copy))};
        for (packets & temp_packets : next_step)
        {
            temp_packets.push_back(p);
            ret.push_back(std::move(temp_packets));
        }
    }
    return ret;
}

std::vector<packets> get_possible_packets(unsigned food, std::vector<good> && other_goods)
{
    auto ret {get_possible_packets_impl(food, std::move(other_goods))};
    // maybe not necessary...
    std::for_each(ret.begin(), ret.end(), [](packets & p)
        { std::sort(p.begin(), p.end()); });
    std::sort(ret.begin(), ret.end());
    auto const del {std::unique(ret.begin(), ret.end())};
    ret.erase(del, ret.end());
    return ret;
}
