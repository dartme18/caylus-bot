#include "stdafx.hpp"

#include "caylus_board.hpp"
#include "caylus_ai.hpp"
#include "util.hpp"

int main(int /*argc*/, char const ** /*args*/)
{
    caylus_ai ai;
    caylus_board b {{0, 1, 2, 3, 4}, get_pink_buildings()};
    move const m {ai.get_move(b)};
    INFO("move {}.", to_string(m));
    set_thread_name("main-thread");
    return 0;
}
