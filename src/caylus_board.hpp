#pragma once
#include "util.hpp"

enum struct phase;
enum struct castle_subphase;

struct caylus_board
{
    caylus_board(std::vector<unsigned> && initial_player_order, std::vector<building_name> && pink_buildings);
    void apply_move(move const &);
    // If move is something other than a union, this will need to change
    std::vector<move> get_possible_moves(void) const;
    std::vector<unsigned> const & get_money(void) const;
    std::vector<unsigned> const & get_move_order(void) const;
    std::vector<unsigned> const & get_goods(good) const;
    std::vector<std::pair<spot_name, building>> const & get_buildings(void) const;
    std::vector<unsigned> const & get_prestige(void) const;

private:
    std::vector<unsigned> move_order;
    std::vector<unsigned> prestige;
    std::vector<std::pair<spot_name, building>> buildings;
    std::vector<unsigned> money;
    std::vector<unsigned> wood;
    std::vector<unsigned> food;
    std::vector<unsigned> stone;
    std::vector<unsigned> cloth;
    std::vector<unsigned> gold;
    std::vector<unsigned> prestige_favor_track;
    std::vector<unsigned> money_favor_track;
    std::vector<unsigned> goods_favor_track;
    std::vector<unsigned> building_favor_track;
    std::vector<unsigned> dungeon_packets;
    std::vector<unsigned> wall_packets;
    std::vector<unsigned> tower_packets;
    phase current_phase {0};
    spot_name bailiff {spot_name::pink5};
    spot_name provost {spot_name::pink5};
    unsigned turn;
    spot_name activation_spot;
    std::vector<unsigned> packets_submitted;
    castle_subphase subphase {0};

    void activate_gate(move const &);
    void income_phase(void);
    void place_a_worker(move const &);
    void move_provost(provost_move const &);
    void get_favor(favor_move const &);
    std::vector<move> get_place_worker_moves(void) const;
    std::vector<move> get_activate_builtin_moves(void) const;
    std::vector<move> get_gate_moves(void) const;
    std::vector<move> get_merchant_moves(void) const;
    std::vector<move> get_joust_field_moves(void) const;
    std::vector<move> get_bridge_moves(void) const;
    std::vector<move> get_activate_buildings_moves(void) const;
    std::vector<move> get_collection_moves(unsigned, std::pair<spot_name, building> const &) const;
    std::vector<move> get_market_moves(unsigned, std::pair<spot_name, building> const &) const;
    std::vector<move> get_carpenter_moves(unsigned) const;
    std::optional<move> get_carpenter_move(unsigned, std::vector<good> const &, building_name) const;
    std::vector<move> get_castle_moves(void) const;
    void next_phase(void);
    void place_worker_next_turn(void);
    void start_activate_buildings_phase(void);
    void start_activate_builtins_phase(void);
    void start_activate_bridge_phase(void);
    void start_activate_castle_phase(void);
    void continue_activate_builtins(move const & m);
    void continue_activate_bridge(move const &);
    void continue_activate_buildings(move const &);
    bool activate_one_castle(move const &);
    void continue_activate_castle(move const &);
    unsigned workers_placed(unsigned) const;
    void activate_trading_post(move const &);
    void activate_merchant_guild(move const &);
    void activate_joust_field(move const &);
    void activate_stable(move const &);
    void activate_inn(move const &);
    bool activate_market(unsigned, move const &, unsigned, unsigned);
    bool activate_pedlar(unsigned, move const &, unsigned, unsigned);
    bool activate_carpenter(unsigned player, move const &);
    bool activate_collection(unsigned player, std::pair<spot_name, building> const &, move const &);
    bool build(building_name, good = good::none);
    void transfer_good_to(unsigned, good, int = 1);
    void trade_for_favor(favor_move const &);
    void transfer_good(unsigned, good, int);
    void build_wood_building(building_move const &);
    unsigned building_count(building_name) const;
    // This function is named so as not to cover the public const one.
    std::vector<unsigned> & get_real_goods(good);
    bool transfer_picked_good(unsigned, move const &, std::vector<good> const &);
    spot_name get_new_building_destination(void) const;
    bool has_goods(unsigned, std::vector<good> const &) const;
    bool can_create_packet(unsigned) const;
    std::vector<std::vector<packet>> get_possible_packets(unsigned) const;
};
