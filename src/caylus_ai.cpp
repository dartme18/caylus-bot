#include "stdafx.hpp"

#include "caylus_ai.hpp"
#include "util.hpp"
#include "caylus_board.hpp"

move caylus_ai::get_move(caylus_board const & b)
{
    auto const & moves {b.get_possible_moves()};
    if (moves.size())
        return moves[0];
    return {};
}

std::vector<move> caylus_ai::get_possible_moves(caylus_board const &)
{
    /*switch(board.get_phase())
    {
        case 0: ERROR("phase 0 is silly"); return {}; // income phase
        case 1: return phase_one_moves(); // place workers
        case 2: ERROR("phase 2 is silly"); return {}; // collect goods
        case 3: return phase_three_moves(); // packets to castle
        case 4: return phase_four_moves();
    }*/
    return {};
}
