#pragma once
#include "util.hpp"

struct caylus_ai
{
    move get_move(struct caylus_board const &);

private:
    std::vector<move> get_possible_moves(caylus_board const &);
};
