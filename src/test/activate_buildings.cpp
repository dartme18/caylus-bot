#include "../stdafx.hpp"

#include "gtest/gtest.h"
#include "../util.hpp"
#include "../caylus_board.hpp"
#include "testing_utils.hpp"

static std::vector<move>::const_iterator find_move(std::vector<move> const & moves, good g)
{
    return std::find_if(moves.begin(), moves.end(), [g](move const & m)
        {
            if (!std::holds_alternative<pick_good>(m))
                return false;
            return std::get<pick_good>(m).picked == g;
        });
}

static std::vector<move>::const_iterator find_move(std::vector<move> const & moves, building_name b)
{
    return std::find_if(moves.begin(), moves.end(), [b](move const & m)
        {
            if (!std::holds_alternative<building_move>(m))
                return false;
            return std::get<building_move>(m).to_build == b;
        });
}

static std::vector<std::pair<spot_name, building>>::const_iterator get_spot(std::vector<std::pair<spot_name, building>> const & buildings, spot_name s)
{
    return std::find_if(buildings.begin(), buildings.end(), [s](std::pair<spot_name, building> const & b)
        { return b.first == s; });
}

TEST(activateBuildings, pinkFoodCloth)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink2}); // food_cloth
    b.apply_move(place_worker {0, spot_name::pink3}); // market
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    b.apply_move(provost_move {1, 0});
    b.apply_move(provost_move {0, 0});
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 2) << "Only choices are pick food or cloth";
    EXPECT_NE(find_move(moves, good::food), moves.end()) << "Food is an option";
    EXPECT_NE(find_move(moves, good::cloth), moves.end()) << "Cloth is an option";
    auto old_food {b.get_goods(good::food)};
    auto old_cloth {b.get_goods(good::cloth)};
    EXPECT_THROW(b.apply_move(pick_good {0, good::cloth}), std::invalid_argument) << "Not player 0's turn";
    b.apply_move(pick_good {1, good::cloth});
    auto new_food {b.get_goods(good::food)};
    auto new_cloth {b.get_goods(good::cloth)};
    EXPECT_EQ(new_food.at(1) - old_food.at(1), 0) << "Didn't pick food";
    EXPECT_EQ(new_cloth.at(1) - old_cloth.at(1), 1) << "Should receive cloth";
}

TEST(activateBuildings, pinkWood)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    unsigned p1_wood {b.get_goods(good::wood).at(1)};
    b.apply_move(place_worker {1, spot_name::pink0}); // wood
    b.apply_move(place_worker {0, spot_name::pink2}); // cloth and food
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    b.apply_move(provost_move {1, 0});
    b.apply_move(provost_move {0, 0});
    EXPECT_EQ(b.get_goods(good::wood).at(1) - p1_wood, 1) << "Player 1 should have earned one wood from the pink building.";
}

TEST(activateBuildings, pinkMarket)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink3}); // market
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    b.apply_move(provost_move {0, 0});
    b.apply_move(provost_move {1, 0});
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 3) << "Can sell food, wood, or nothing";
    EXPECT_NE(find_move(moves, good::food), moves.end());
    EXPECT_NE(find_move(moves, good::wood), moves.end());
    EXPECT_NE(find_move(moves, good::none), moves.end()) << "Should be able to decline market activation";
}

// Disabled until we can get past some building activation
TEST(activateBuildings, DISABLED_fixedPedlar)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::fixed_pedlar}); // market
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    b.apply_move(provost_move {0, 0});
    b.apply_move(provost_move {1, 0});
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 2) << "Can sell food or wood";
    EXPECT_NE(find_move(moves, good::food), moves.end());
    EXPECT_NE(find_move(moves, good::wood), moves.end());
    auto old_food {b.get_goods(good::food)};
    auto old_money {b.get_money()};
    b.apply_move(pick_good {1, good::food});
    auto new_food {b.get_goods(good::food)};
    auto new_money {b.get_money()};
    EXPECT_EQ(new_food.at(1), old_food.at(1) + 1) << "Get one food from market";
    EXPECT_EQ(new_money.at(1), old_money.at(1) - 4) << "Costs four coins";
}

void try_build_building_with_carpenter(building_name building)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink4}); // carpenter
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    b.apply_move(provost_move {0, 0});
    b.apply_move(provost_move {1, 0});
    auto moves {b.get_possible_moves()};
    for (auto const & m : moves)
    {
        EXPECT_TRUE(std::holds_alternative<building_move>(m)) << "It's time to build a building, so only building moves should be available";
        EXPECT_EQ(get_building_type(std::get<building_move>(m).to_build), building_type::wood) << "Only wood buildings can be built at the carpenter";
    }
    std::vector<unsigned> old_prestige {b.get_prestige()};
    b.apply_move(*find_move(moves, building));
    std::vector<unsigned> new_prestige {b.get_prestige()};
    auto const & buildings {b.get_buildings()};
    auto spot {get_spot(buildings, spot_name::build0)};
    EXPECT_NE(spot, buildings.end()) << "build0 spot should be among the buildings";
    EXPECT_EQ(spot->second.name, building) << "The building wasn't built";
    EXPECT_NE(prestige_from_build(building), 0) << "Building should give some prestige";
    EXPECT_EQ(new_prestige.at(1), old_prestige.at(1) + prestige_from_build(building)) << "Incorrect prestige awarded";
}

TEST(activateBuildings, masonAtCarpenter)
{
    try_build_building_with_carpenter(building_name::mason);
}

TEST(activateBuildings, woodFoodFarmAtCarpenter)
{
    try_build_building_with_carpenter(building_name::wood_food_farm);
}

TEST(activateBuildings, woodClothFarmAtCarpenter)
{
    try_build_building_with_carpenter(building_name::wood_cloth_farm);
}

TEST(activateBuildings, noLawyerAtCarpenter)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink4}); // carpenter
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    b.apply_move(provost_move {0, 0});
    b.apply_move(provost_move {1, 0});
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(find_move(moves, building_name::lawyer), moves.end()) << "Can't build lawyer: don't have stone yet";
}

TEST(activateBuildings, DISABLED_lawyerAtCarpenter)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink3}); // market
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::fixed_carpenter});
    b.apply_move(place_worker {1, spot_name::bridge1});
    b.apply_move(provost_move {0, 0});
    b.apply_move(provost_move {1, 0});
    b.apply_move(pick_good {1, good::stone}); // buy some stone
    auto moves {b.get_possible_moves()};
    for (auto const & m : moves)
    {
        EXPECT_TRUE(std::holds_alternative<building_move>(m)) << "It's time to build a building, so only building moves should be available";
        EXPECT_EQ(get_building_type(std::get<building_move>(m).to_build), building_type::wood) << "Only wood buildings can be built at the carpenter";
    }
    std::vector<unsigned> old_prestige {b.get_prestige()};
    b.apply_move(*find_move(moves, building_name::lawyer));
    std::vector<unsigned> new_prestige {b.get_prestige()};
    auto const & buildings {b.get_buildings()};
    auto spot {get_spot(buildings, spot_name::build0)};
    EXPECT_NE(spot, buildings.end()) << "build0 spot should be among the buildings";
    EXPECT_EQ(spot->second.name, building_name::lawyer) << "The building wasn't built";
    EXPECT_NE(prestige_from_build(building_name::lawyer), 0) << "Building should give some prestige";
    EXPECT_EQ(new_prestige.at(1), old_prestige.at(1) + prestige_from_build(building_name::lawyer)) << "Incorrect prestige awarded";
}

TEST(activateBuildings, marketplaceAtCarpenter)
{
    try_build_building_with_carpenter(building_name::marketplace);
}

TEST(activateBuildings, pedlarAtCarpenter)
{
    try_build_building_with_carpenter(building_name::pedlar);
}

// Disabled until we can play long enough not to have any goods
// If a user has no goods to sell at the market, he shouldn't be
// given a choice of what to do.
TEST(activateBuildings, DISABLED_ifNoGoodsNoMarket)
{
}

// Disabled until we can get this far into the game
// Scenarios like: build two different buildings that create favors
//     activate jousting field, build a building that offers a favor
//     get favor from castle building then build a building that offers favor
//     get favor from castle building and score castle getting favors
//          I think this should allow multiple favors on same track
//     get multiple favors from scoring castle
TEST(activateBuildings, DISABLED_cantMultipleFavorsSameTrackDuringPhase)
{
}
