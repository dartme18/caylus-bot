#include "../stdafx.hpp"

#include "gtest/gtest.h"
#include "../util.hpp"
#include "../caylus_board.hpp"
#include "testing_utils.hpp"

static std::vector<move>::const_iterator find_move(std::vector<move> const & moves, spot_name spot)
{
    return std::find_if(moves.begin(), moves.end(), [spot](move const & m)
        {
            if (!std::holds_alternative<place_worker>(m))
                return false;
            return std::get<place_worker>(m).spot == spot;
        });
}

static std::vector<move>::const_iterator find_favor_money_move(std::vector<move> const & moves, unsigned money)
{
    return std::find_if(moves.begin(), moves.end(), [money](move const & m)
        {
            if (!std::holds_alternative<favor_move>(m))
                return false;
            favor_move const & real_move {std::get<favor_move>(m)};
            return real_move.track == favor_track::money && real_move.column == money - 2;
        });
}

/*
 * Needed for jousting field
static std::vector<move>::const_iterator find_move(std::vector<move> const & moves, good pick)
{
    return std::find_if(moves.begin(), moves.end(), [pick](move const & m) {
        if (!std::holds_alternative<pick_good>(m))
            return false;
        return std::get<pick_good>(m).picked == pick;
    });
}
static std::vector<move>::const_iterator find_favor_prestige_move(std::vector<move> const & moves, unsigned prestige)
{
    return std::find_if(moves.begin(), moves.end(), [prestige](move const & m) {
        if (!std::holds_alternative<favor_move>(m))
            return false;
        favor_move const & real_move{std::get<favor_move>(m)};
        return real_move.track == favor_track::prestige && real_move.column == prestige;
    });
}

static std::vector<move>::const_iterator find_favor_move(std::vector<move> const & moves, good g)
{
    return std::find_if(moves.begin(), moves.end(), [g](move const & m) {
        if (!std::holds_alternative<favor_move>(m))
            return false;
        favor_move const & real_move{std::get<favor_move>(m)};
        return real_move.track == favor_track::goods && real_move.receiving0 == g;
    });
}

static std::vector<move>::const_iterator find_favor_move(std::vector<move> const & moves, good give, good rec0, good rec1)
{
    return std::find_if(moves.begin(), moves.end(), [give, rec0, rec1](move const & m) {
        if (!std::holds_alternative<favor_move>(m))
            return false;
        favor_move const & real_move{std::get<favor_move>(m)};
        return real_move.track == favor_track::goods && real_move.receiving0 == rec0 && real_move.giving == give && real_move.receiving1 == rec1;
    });
}

static std::vector<move>::const_iterator find_favor_move(std::vector<move> const & moves, favor_track track)
{
    return std::find_if(moves.begin(), moves.end(), [track](move const & m) {
        if (!std::holds_alternative<favor_move>(m))
            return false;
        return std::get<favor_move>(m).track == track;
    });
}
*/

TEST(activateBuiltIns, gateCantPlaceOnOthers)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::gate});
    b.apply_move(place_worker {0, spot_name::pink0});
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    auto moves {b.get_possible_moves()};
    // All these moves should be available to move from the gate
    EXPECT_NE(find_move(moves, spot_name::trading_post), moves.end());
    EXPECT_NE(find_move(moves, spot_name::merchant_guild), moves.end());
    EXPECT_NE(find_move(moves, spot_name::joust_field), moves.end());
    EXPECT_NE(find_move(moves, spot_name::stables0), moves.end());
    EXPECT_NE(find_move(moves, spot_name::stables1), moves.end());
    EXPECT_NE(find_move(moves, spot_name::inn0), moves.end());
    EXPECT_NE(find_move(moves, spot_name::pink1), moves.end());
    EXPECT_NE(find_move(moves, spot_name::pink2), moves.end());
    EXPECT_NE(find_move(moves, spot_name::pink3), moves.end());
    EXPECT_NE(find_move(moves, spot_name::pink4), moves.end());
    EXPECT_NE(find_move(moves, spot_name::pink5), moves.end());
    EXPECT_NE(find_move(moves, spot_name::fixed_carpenter), moves.end());
    EXPECT_NE(find_move(moves, spot_name::fixed_pedlar), moves.end());
    EXPECT_NE(find_move(moves, spot_name::castle0), moves.end());
    EXPECT_NE(find_move(moves, spot_name::castle1), moves.end());
    EXPECT_NE(find_move(moves, spot_name::castle2), moves.end());
    EXPECT_NE(find_move(moves, spot_name::castle3), moves.end());
    EXPECT_NE(find_move(moves, spot_name::castle4), moves.end());
    // These moves are NOT available from the gate
    EXPECT_EQ(find_move(moves, spot_name::inn1), moves.end()) << "The only way to move into the inn is by taking the inn spot";
    EXPECT_EQ(find_move(moves, spot_name::bridge0), moves.end()) << "Can't move from gate to bridge";
    EXPECT_EQ(find_move(moves, spot_name::bridge1), moves.end()) << "Can't move from gate to bridge";
    EXPECT_EQ(find_move(moves, spot_name::bridge2), moves.end()) << "Can't move from gate to bridge";
    EXPECT_EQ(find_move(moves, spot_name::bridge3), moves.end()) << "Can't move from gate to bridge";
    EXPECT_EQ(find_move(moves, spot_name::bridge4), moves.end()) << "Can't move from gate to bridge";
    EXPECT_EQ(find_move(moves, spot_name::pink0), moves.end()) << "This spot is taken by another player";
    EXPECT_EQ(find_move(moves, spot_name::stables2), moves.end()) << "Can't take stables2 in a two player game";
    EXPECT_EQ(moves.size(), 18);
}

// Disabled until builtin activations are done
TEST(activateBuiltIns, tradingPostGetsMoney)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    std::vector<unsigned> money {b.get_money()};
    b.apply_move(place_worker {1, spot_name::trading_post});
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    std::vector<unsigned> new_money {b.get_money()};
    EXPECT_EQ(new_money[1] - money[1], 3 - 1) << "Should earn three deniers from trading post, "
                                                 "but pay one for placing on trading post";
}

TEST(activateBuiltIns, stables0)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::stables1});
    b.apply_move(place_worker {0, spot_name::stables0});
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    std::vector<unsigned> const & move_order {b.get_move_order()};
    EXPECT_EQ(move_order.size(), 2);
    EXPECT_EQ(move_order[0], 0);
    EXPECT_EQ(move_order[1], 1);
}

TEST(activateBuiltIns, stables1)
{
    caylus_board b {{0, 1}, get_test_pink_buildings()};
    b.apply_move(place_worker {0, spot_name::stables1});
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    std::vector<unsigned> const & move_order {b.get_move_order()};
    EXPECT_EQ(move_order.size(), 2);
    EXPECT_EQ(move_order[0], 1);
    EXPECT_EQ(move_order[1], 0);
}

TEST(activateBuiltIns, cantAffordJoustingFieldFavor)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::joust_field});
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    auto moves {b.get_possible_moves()};
    for (auto const & m : moves)
        // These should be bridge moves
        EXPECT_FALSE(std::holds_alternative<favor_move>(m)) << "Can't buy jousting without cloth and denier";
}

// Disabled until we can play one round to get cloth, then another round to get jousting
TEST(activateBuiltIns, DISABLED_joustingFieldFavor)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink2}); // pink_cloth
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    b.apply_move(place_worker {1, spot_name::joust_field});
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    auto moves {b.get_possible_moves()};
    //EXPECT_NE(find_favor_money_move(moves, 3), moves.end()) << "Should be able to find money favor";
    //EXPECT_NE(find_favor_prestige_move(moves, 1), moves.end()) << "Prestige favor should be available";
    //EXPECT_NE(find_favor_move(moves, good::food), moves.end()) << "Should be able to food by favor";
    //EXPECT_NE(find_favor_move(moves, favor_track::building), moves.end()) << "Should be able to advance on the building favor track";
}

TEST(activateBuiltIns, DISABLED_declineJoustFavor)
{
    // land on jousting, have enough to buy a favor, and decide against it
}

// disabled until jousting field can be used
TEST(activateBuiltIns, DISABLED_joustingFieldMoney)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::joust_field});
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    auto moves {b.get_possible_moves()};
    auto money_spot {find_favor_money_move(moves, 3)};
    ASSERT_NE(money_spot, moves.end()) << "Money spot not found";
    std::vector<unsigned> old_money {b.get_money()};
    b.apply_move(*money_spot);
    std::vector<unsigned> new_money {b.get_money()};
    EXPECT_EQ(new_money[0] - old_money[0], 1) << "Player 0 got one from the bridge";
    EXPECT_EQ(new_money[1] - old_money[1], 3) << "Player 1 got three as a favor";
}

TEST(activateBuiltIns, merchantGuildMoveProvost)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::merchant_guild});
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::bridge1});
    auto const moves {b.get_possible_moves()};
}

TEST(activateBuiltIns, DISABLED_gateGetsPrestigePointsForOtherPlayers)
{
    // create some buildings, move from the gate to those owned buildings, other player gets prestige
}

// Disabled until we have the end of the board
TEST(activateBuiltIns, DISABLED_cantMoveProvostPastEnd)
{
}

// Disabled until multiple rounds can be played to demonstrate that I'm in the inn
TEST(activateBuiltIns, DISABLED_playOnInnGetInInn)
{
}

// Show that it only costs one denier to place a worker after you have a worker in the inn
TEST(activateBuiltins, DISABLED_innWorks)
{
}

// Disabled until multiple rounds can be played so that I can place on the inn and get my guy back.
TEST(activateBuiltIns, DISABLED_canGetPlayerBackFromInn)
{
}

// With one worker on the inn, you can only place five other workers (including one on the bridge
TEST(activateBuiltins, DISABLED_innTakesWorker)
{
}
