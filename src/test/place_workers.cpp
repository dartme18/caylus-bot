#include "../stdafx.hpp"

#include "gtest/gtest.h"
#include "../util.hpp"
#include "../caylus_board.hpp"
#include "testing_utils.hpp"

// Don't use underscores in test suites or test names.
TEST(placeWorker, correctStartingSpots)
{
    caylus_board b {{1, 0, 2}, get_test_pink_buildings()};
    std::vector<move> const moves {b.get_possible_moves()};
    EXPECT_PRED1(get_tester(spot_name::pink0), moves) << "Missing pink building";
    EXPECT_PRED1(get_tester(spot_name::pink1), moves);
    EXPECT_PRED1(get_tester(spot_name::pink2), moves);
    EXPECT_PRED1(get_tester(spot_name::pink3), moves);
    EXPECT_PRED1(get_tester(spot_name::pink4), moves);
    EXPECT_PRED1(get_tester(spot_name::pink5), moves);
    EXPECT_PRED1(get_tester(spot_name::bridge0), moves);
    EXPECT_EQ(get_bridge_count(moves), 1);
    EXPECT_FALSE(get_tester(spot_name::none)(moves)) << "\"None\" building should not be present";
}

// Disabled until we can play enough to have enough money to place all the workers
TEST(placeWorker, DISABLED_limitWorkerCount)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink0});
    b.apply_move(place_worker {0, spot_name::pink1});
    b.apply_move(place_worker {1, spot_name::pink2});
    b.apply_move(place_worker {0, spot_name::pink3});
    b.apply_move(place_worker {1, spot_name::pink4});
    b.apply_move(place_worker {0, spot_name::pink5});
    b.apply_move(place_worker {1, spot_name::gate});
    b.apply_move(place_worker {0, spot_name::trading_post});
    b.apply_move(place_worker {1, spot_name::merchant_guild});
    b.apply_move(place_worker {0, spot_name::joust_field});
    b.apply_move(place_worker {1, spot_name::stables0});
    b.apply_move(place_worker {0, spot_name::stables1});
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 1) << "After playing six times, the only possible move now is pass";
    EXPECT_THROW(b.apply_move(place_worker {1, spot_name::inn0}), std::invalid_argument) << "Player 1 can't go a seventh time";
    b.apply_move(moves[0]);
    moves = b.get_possible_moves();
    EXPECT_EQ(moves.size(), 1) << "After playing six times, the only possible move now is pass";
    EXPECT_THROW(b.apply_move(place_worker {1, spot_name::inn0}), std::invalid_argument) << "Player 0 can't go a seventh time";
    b.apply_move(moves[0]);
}

// Inn1 can only be occupied after activating from inn0
TEST(placeWorker, cannotPlaceOnInn1)
{
    caylus_board b {{0, 1}, get_test_pink_buildings()};
    EXPECT_THROW(b.apply_move(place_worker {0, spot_name::inn1}), std::invalid_argument) << "Should not be able to place on inn1";
}

TEST(placeWorker, onlyOneAtStable)
{
    caylus_board b {{0, 1, 2}, get_test_pink_buildings()};
    b.apply_move(place_worker {0, spot_name::stables0});
    b.apply_move(place_worker {1, spot_name::stables1});
    b.apply_move(place_worker {2, spot_name::stables2});
    auto const & moves {b.get_possible_moves()};
    EXPECT_FALSE(get_tester(spot_name::stables0)(moves)) << "A player can't go on the stables twice.";
    EXPECT_FALSE(get_tester(spot_name::stables1)(moves)) << "A player can't go on the stables twice.";
    EXPECT_FALSE(get_tester(spot_name::stables2)(moves)) << "A player can't go on the stables twice.";
}

TEST(placeWorker, submitMove)
{
    caylus_board b {{1, 0, 2}, get_test_pink_buildings()};
    move m {place_worker {1, spot_name::pink3}};
    b.apply_move(m);
    auto const moves {b.get_possible_moves()};
    EXPECT_FALSE(get_tester(spot_name::pink3)(moves)) << "This spot should not be available after just taking it";
}

TEST(placeWorker, turnEnforcement)
{
    caylus_board b {{1, 0, 2}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink2});
    EXPECT_THROW(b.apply_move(place_worker {1, spot_name::pink2}), std::invalid_argument) << "Player 1 can't go twice";
}

TEST(placeWorker, firstPassDenier)
{
    caylus_board b {{1, 0, 2}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink2});
    EXPECT_EQ(b.get_money()[1], 4) << "Starts with 5, costs 1 to place";
    EXPECT_EQ(b.get_money()[0], 6);
    EXPECT_EQ(b.get_money()[2], 6);
    b.apply_move(place_worker {0, spot_name::bridge0});
    EXPECT_EQ(b.get_money()[1], 4);
    EXPECT_EQ(b.get_money()[0], 7) << "First pass gets one denier";
    EXPECT_EQ(b.get_money()[2], 6);
}

TEST(placeWorker, correctBridgeSpots)
{
    caylus_board b {{1, 0, 2}, get_test_pink_buildings()};
    auto moves {b.get_possible_moves()};
    EXPECT_NE(find_building_move(moves, spot_name::bridge0), moves.end()) << "Bridge0 should be available at the beginning";
    EXPECT_EQ(find_building_move(moves, spot_name::bridge1), moves.end()) << "Bridge1 is not available for the first move";
    EXPECT_EQ(find_building_move(moves, spot_name::bridge2), moves.end()) << "Bridge2 is not available for the first move";
    EXPECT_EQ(find_building_move(moves, spot_name::bridge3), moves.end()) << "Bridge3 is not available for the first move";
    EXPECT_EQ(find_building_move(moves, spot_name::bridge4), moves.end()) << "Bridge4 is not available for the first move";
    b.apply_move(place_worker {1, spot_name::bridge0});
    moves = b.get_possible_moves();
    EXPECT_EQ(find_building_move(moves, spot_name::bridge0), moves.end()) << "Bridge0 is no longer available";
    EXPECT_NE(find_building_move(moves, spot_name::bridge1), moves.end()) << "Bridge1 is now available!";
    EXPECT_EQ(find_building_move(moves, spot_name::bridge2), moves.end()) << "Bridge2 is not available now";
    EXPECT_EQ(find_building_move(moves, spot_name::bridge3), moves.end()) << "Bridge3 is not available now";
    EXPECT_EQ(find_building_move(moves, spot_name::bridge4), moves.end()) << "Bridge4 is not available now";
}

// Disabled until the Jousting field works (and probably all the fixed buildings before the bridge)
TEST(placeWorker, DISABLED_placeAllWorkers)
{
    caylus_board b {{1, 0, 2}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink2});
    b.apply_move(place_worker {0, spot_name::pink0});
}

TEST(placeWorker, canPlaceOnStable2)
{
    caylus_board b {{0, 1, 2}, get_test_pink_buildings()};
    auto const & moves {b.get_possible_moves()};
    EXPECT_NE(find_building_move(moves, spot_name::stables2), moves.end()) << "Can place on stable-3 in a three-player game";
    b.apply_move(place_worker {0, spot_name::stables2});
}

TEST(placeWorker, twoPlayerCannotPlaceOnThirdStable)
{
    caylus_board b {{0, 1}, get_test_pink_buildings()};
    auto const & moves {b.get_possible_moves()};
    EXPECT_EQ(find_building_move(moves, spot_name::stables2), moves.end()) << "Cannot place on stable-3 in a two-player game";
    EXPECT_THROW(b.apply_move(place_worker {0, spot_name::stables2}), std::invalid_argument);
}

TEST(placeWorker, costsMoneyToPlace)
{
    caylus_board b {{1, 0, 2}, get_test_pink_buildings()};
    auto money {b.get_money()};
    ASSERT_EQ(money.size(), 3) << "There better be money for each player";
    EXPECT_EQ(money[1], 5) << "First player starts with 5";
    EXPECT_EQ(money[0], 6) << "Second player starts with 6";
    EXPECT_EQ(money[2], 6) << "Third player starts with 6";
    b.apply_move(place_worker {1, spot_name::pink2});
    money = b.get_money();
    ASSERT_EQ(money.size(), 3) << "There better be money for each player";
    EXPECT_EQ(money[1], 4) << "First player starts with 5, then it costs one to place";
    EXPECT_EQ(money[0], 6) << "Second player starts with 6";
    EXPECT_EQ(money[2], 6) << "Third player starts with 6";
    b.apply_move(place_worker {0, spot_name::bridge0});
    money = b.get_money();
    ASSERT_EQ(money.size(), 3) << "There better be money for each player";
    EXPECT_EQ(money[1], 4) << "Still 4";
    EXPECT_EQ(money[0], 7) << "Second player gets a coin to pass first";
    EXPECT_EQ(money[2], 6) << "Third player starts with 6";
    b.apply_move(place_worker {2, spot_name::pink1});
    money = b.get_money();
    ASSERT_EQ(money.size(), 3) << "There better be money for each player";
    EXPECT_EQ(money[1], 4) << "Still 4";
    EXPECT_EQ(money[0], 7) << "still 7";
    EXPECT_EQ(money[2], 4) << "Pay 2 to place after second player passed";
}

TEST(placeWorker, boardDoesntSuggestPlacementICantAfford)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink0});
    b.apply_move(place_worker {0, spot_name::pink1});
    b.apply_move(place_worker {1, spot_name::pink2});
    b.apply_move(place_worker {0, spot_name::pink3});
    b.apply_move(place_worker {1, spot_name::pink4});
    b.apply_move(place_worker {0, spot_name::pink5});
    b.apply_move(place_worker {1, spot_name::gate});
    b.apply_move(place_worker {0, spot_name::trading_post});
    b.apply_move(place_worker {1, spot_name::merchant_guild});
    b.apply_move(place_worker {0, spot_name::joust_field});
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 1) << "I have no more money, the only possible move now is pass";
    EXPECT_THROW(b.apply_move(place_worker {1, spot_name::inn0}), std::invalid_argument) << "Player 1 has no money left";
    b.apply_move(moves[0]); // player 1 pass
    moves = b.get_possible_moves(); //player 0 moves
    EXPECT_EQ(moves.size(), 1) << "Even though I have one denier, since it costs 2 to move now, I can only pass";
    EXPECT_THROW(b.apply_move(place_worker {0, spot_name::inn0}), std::invalid_argument) << "Player 0 has no money left";
    b.apply_move(moves[0]);
}

TEST(placeWorker, twiceInARowIfOthersPassed)
{
    caylus_board b {{1, 0, 2, 3}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    b.apply_move(place_worker {2, spot_name::pink0});
    b.apply_move(place_worker {3, spot_name::bridge2});
    // It's 2's turn again. If move ordering during place worker is incorrect, this will likely throw.
    b.apply_move(place_worker {2, spot_name::bridge3});
}
