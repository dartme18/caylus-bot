#include "../stdafx.hpp"

#include "gtest/gtest.h"
#include "../util.hpp"
#include "../caylus_board.hpp"

std::function<bool(std::vector<move> const &)> get_tester(spot_name expected)
{
    // Returns true if there is a match
    return [expected](std::vector<move> const & ms)
    {
        return std::find_if(ms.begin(), ms.end(), [expected](move const & m)
                   {
                       if (!std::holds_alternative<place_worker>(m))
                           return false;
                       return std::get<place_worker>(m).spot == expected;
                   })
            != ms.end();
    };
}

std::vector<move>::iterator::difference_type get_bridge_count(std::vector<move> const & moves)
{
    return std::count_if(moves.begin(), moves.end(), [](move const & m)
        {
            if (!std::holds_alternative<place_worker>(m))
                return false;
            return is_bridge(std::get<place_worker>(m).spot);
        });
}

std::vector<move>::const_iterator find_building_move(std::vector<move> const & m, spot_name n)
{
    return std::find_if(m.begin(), m.end(), [n](move const & tempm)
        {
            if (!std::holds_alternative<place_worker>(tempm))
                return false;
            return std::get<place_worker>(tempm).spot == n;
        });
}

std::vector<building_name> get_test_pink_buildings(void)
{
    return {building_name::pink_wood, building_name::pink_stone, building_name::pink_cloth,
        building_name::pink_market, building_name::carpenter, building_name::pink_food};
}
