#include "../stdafx.hpp"

#include "gtest/gtest.h"
#include "../caylus_board.hpp"
#include "testing_utils.hpp"

std::vector<std::vector<packet>>::const_iterator find(std::vector<std::vector<packet>> const & all_packets, std::vector<good> & packets)
{
    std::sort(packets.begin(), packets.end());
    return std::find_if(all_packets.begin(), all_packets.end(), [&packets](std::vector<packet> const & test)
        {
            if (test.size() != packets.size() / 2)
                return false;
            std::vector<good> searching;
            std::for_each(test.begin(), test.end(), [&searching](packet const & p)
                {
                    searching.push_back(p[0]);
                    searching.push_back(p[1]);
                });
            std::sort(searching.begin(), searching.end());
            return std::equal(searching.begin(), searching.end(), packets.begin());
        });
}

TEST(castle, notEnoughGoods)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::castle0});
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {1, spot_name::pink1}); // stone
    b.apply_move(place_worker {1, spot_name::bridge1});
    b.apply_move(provost_move {0, 0});
    b.apply_move(provost_move {1, 0});
    EXPECT_EQ(1u / 3u, 0);
    EXPECT_EQ(2u / 3u, 0);
    EXPECT_EQ(3u / 3u, 1);
    auto moves {b.get_possible_moves()};
}
