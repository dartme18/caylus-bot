#include "../util.hpp"

std::function<bool(std::vector<move> const &)> get_tester(spot_name expected);
std::vector<move>::iterator::difference_type get_bridge_count(std::vector<move> const & moves);
std::vector<move>::const_iterator find_building_move(std::vector<move> const &, spot_name);
std::vector<building_name> get_test_pink_buildings(void);
