#include "../stdafx.hpp"

#include "gtest/gtest.h"
#include "../util.hpp"
#include "../caylus_board.hpp"
#include "testing_utils.hpp"

TEST(utils, correctDefaultMove)
{
    move const m {};
    EXPECT_TRUE(std::holds_alternative<no_move>(m));
    EXPECT_FALSE(std::holds_alternative<place_worker>(m));
    auto const & real_move {std::get<no_move>(m)};
    EXPECT_EQ(real_move.player, 255);
}

TEST(board, correctStartingDeniersTwoPlayer)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    auto deniers {b.get_money()};
    ASSERT_EQ(deniers.size(), 2) << "There better be deniers for each player";
    EXPECT_EQ(deniers[1], 5) << "First player starts with 5";
    EXPECT_EQ(deniers[0], 6) << "Second player starts with 6";
}

TEST(board, correctStartingDeniersThreePlayer)
{
    caylus_board b {{2, 1, 0}, get_test_pink_buildings()};
    auto deniers {b.get_money()};
    ASSERT_EQ(deniers.size(), 3) << "There better be deniers for each player";
    EXPECT_EQ(deniers[2], 5) << "First player starts with 5";
    EXPECT_EQ(deniers[1], 6) << "First player starts with 5";
    EXPECT_EQ(deniers[0], 6) << "Second player starts with 6";
}

TEST(board, correctStartingDeniersFivePlayer)
{
    caylus_board b {{0, 3, 2, 1, 4}, get_test_pink_buildings()};
    auto deniers {b.get_money()};
    ASSERT_EQ(deniers.size(), 5) << "There better be deniers for each player";
    EXPECT_EQ(deniers[0], 5) << "First player starts with 5";
    EXPECT_EQ(deniers[3], 6) << "First player starts with 5";
    EXPECT_EQ(deniers[2], 6) << "Second player starts with 6";
    EXPECT_EQ(deniers[1], 7) << "Second player starts with 6";
    EXPECT_EQ(deniers[4], 7) << "Second player starts with 6";
}

void assert_all_unique(std::vector<packet> const & packets)
{
    for (auto const & p : packets)
    {
        ASSERT_EQ(p.size(), 2);
        EXPECT_NE(p[0], p[1]) << "Packet contents must be unique";
    }
}

bool has_packet(packet const & p, packet const & test)
{
    return (p[0] == test[0] && p[1] == test[1]) || (p[0] == test[1] && p[1] == test[0]);
}

bool has_packet(std::vector<packet> const & packets, std::array<good, 2> const & packet)
{
    for (auto const & temp_packet : packets)
        if (has_packet(temp_packet, packet))
            return true;
    return false;
}

bool has_packets(std::vector<std::vector<packet>> const & all_packets, std::vector<good> && packets)
{
    std::sort(packets.begin(), packets.end());
    for (auto const & set : all_packets)
    {
        std::vector<good> all_packet_goods;
        std::for_each(set.begin(), set.end(), [&all_packet_goods](packet const & p)
            {
                all_packet_goods.push_back(p[0]);
                all_packet_goods.push_back(p[1]);
            });
        std::sort(all_packet_goods.begin(), all_packet_goods.end());
        if (all_packet_goods.size() != packets.size())
            continue;
        if (std::equal(all_packet_goods.begin(), all_packet_goods.end(), packets.begin()))
            return true;
    }
    return false;
}

TEST(utils, get_single_packets)
{
    std::vector<good> goods {good::wood, good::wood, good::cloth, good::cloth, good::stone, good::gold};
    auto packets {get_all_unique_packets(goods)};
    ASSERT_EQ(packets.size(), 6) << "There should be six different options for packets to deliver to the castle";
    EXPECT_TRUE(has_packet(packets, {good::wood, good::cloth}));
    EXPECT_TRUE(has_packet(packets, {good::wood, good::stone}));
    EXPECT_TRUE(has_packet(packets, {good::wood, good::gold}));
    EXPECT_TRUE(has_packet(packets, {good::cloth, good::stone}));
    EXPECT_TRUE(has_packet(packets, {good::cloth, good::gold}));
    EXPECT_TRUE(has_packet(packets, {good::stone, good::gold}));
}

TEST(utils, get_simple_packets0)
{
    std::vector<good> goods {good::wood, good::cloth, good::cloth};
    auto packets {get_possible_packets(2, std::move(goods))};
    ASSERT_EQ(packets.size(), 2);
    EXPECT_TRUE(has_packets(packets, {}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::cloth}));
}

TEST(utils, get_simple_packets1)
{
    std::vector<good> goods {good::wood, good::cloth, good::cloth};
    auto packets {get_possible_packets(1, std::move(goods))};
    ASSERT_EQ(packets.size(), 2);
    EXPECT_TRUE(has_packets(packets, {}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::cloth}));
}

TEST(utils, get_packets)
{
    std::vector<good> goods {good::wood, good::wood, good::cloth, good::cloth, good::stone, good::gold};
    auto packets {get_possible_packets(3, std::move(goods))};
    ASSERT_EQ(packets.size(), 16);
    EXPECT_TRUE(has_packets(packets, {}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::stone}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::cloth}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::gold}));
    EXPECT_TRUE(has_packets(packets, {good::stone, good::cloth}));
    EXPECT_TRUE(has_packets(packets, {good::stone, good::gold}));
    EXPECT_TRUE(has_packets(packets, {good::cloth, good::gold}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::stone, good::wood, good::cloth}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::stone, good::wood, good::gold}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::cloth, good::wood, good::cloth}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::cloth, good::wood, good::gold}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::cloth, good::stone, good::cloth}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::gold, good::stone, good::cloth}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::cloth, good::cloth, good::gold}));
    EXPECT_TRUE(has_packets(packets, {good::stone, good::cloth, good::cloth, good::gold}));
    EXPECT_TRUE(has_packets(packets, {good::wood, good::cloth, good::wood, good::gold, good::stone, good::cloth}));
}

TEST(utils, packet_equality0)
{
    packet good0 {good::wood, good::cloth};
    packet good1 {good::cloth, good::wood};
    EXPECT_EQ(good0, good1) << "Should be using custom == operator";
    EXPECT_EQ(good1, good0) << "Should be using custom == operator";
}

TEST(utils, packets_equality1)
{
    packet good0 {good::cloth, good::wood};
    packet good1 {good::cloth, good::wood};
    EXPECT_EQ(good1, good0) << "Should be using custom == operator";
    EXPECT_EQ(good0, good1) << "Should be using custom == operator";
}
