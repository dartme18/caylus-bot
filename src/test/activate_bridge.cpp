#include "../stdafx.hpp"

#include "gtest/gtest.h"
#include "../util.hpp"
#include "../caylus_board.hpp"
#include "testing_utils.hpp"

static std::vector<move>::const_iterator find_provost_move(std::vector<move> const & moves, int count)
{
    return std::find_if(moves.begin(), moves.end(), [count](move const & m)
        {
            if (!std::holds_alternative<provost_move>(m))
                return false;
            return std::get<provost_move>(m).move_count == count;
        });
}

TEST(activateBridge, mustHaveEnoughMoney)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::fixed_carpenter});
    b.apply_move(place_worker {0, spot_name::pink0});
    b.apply_move(place_worker {1, spot_name::fixed_pedlar});
    b.apply_move(place_worker {0, spot_name::pink1});
    b.apply_move(place_worker {1, spot_name::pink2});
    b.apply_move(place_worker {0, spot_name::pink3});
    b.apply_move(place_worker {1, spot_name::pink4});
    b.apply_move(place_worker {0, spot_name::pink5});
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    auto deniers {b.get_money()};
    ASSERT_EQ(deniers.size(), 2) << "Deniers for each player";
    EXPECT_EQ(deniers[1], 2) << "Start with 5 - 4 placements + 1 first pass";
    EXPECT_EQ(deniers[0], 2) << "Start with 6 - 4 placements";
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 5) << "Back two, forward two, stay";
    EXPECT_NE(find_provost_move(moves, -2), moves.end());
    EXPECT_NE(find_provost_move(moves, -1), moves.end());
    EXPECT_NE(find_provost_move(moves, 0), moves.end());
    EXPECT_NE(find_provost_move(moves, 1), moves.end());
    EXPECT_NE(find_provost_move(moves, 2), moves.end());
}

TEST(activateBridge, lessMoney)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::fixed_carpenter});
    b.apply_move(place_worker {0, spot_name::pink0});
    b.apply_move(place_worker {1, spot_name::fixed_pedlar});
    b.apply_move(place_worker {0, spot_name::pink1});
    b.apply_move(place_worker {1, spot_name::pink2});
    b.apply_move(place_worker {0, spot_name::pink3});
    b.apply_move(place_worker {1, spot_name::pink4});
    b.apply_move(place_worker {0, spot_name::pink5});
    b.apply_move(place_worker {1, spot_name::stables1});
    b.apply_move(place_worker {0, spot_name::stables0});
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    auto deniers {b.get_money()};
    ASSERT_EQ(deniers.size(), 2) << "Deniers for each player";
    EXPECT_EQ(deniers[1], 1) << "Start with 5 - 5 placements + 1 first pass";
    EXPECT_EQ(deniers[0], 1) << "Start with 6 - 5 placements";
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 3) << "Back one, forward one, stay";
    EXPECT_NE(find_provost_move(moves, -1), moves.end());
    EXPECT_NE(find_provost_move(moves, 0), moves.end());
    EXPECT_NE(find_provost_move(moves, 1), moves.end());
}

TEST(activateBridge, cantMoveTooMuchInTwoPlayer)
{
    caylus_board b {{1, 0}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 7) << "Back three, forward three, stay";
    EXPECT_NE(find_provost_move(moves, -3), moves.end());
    EXPECT_NE(find_provost_move(moves, -2), moves.end());
    EXPECT_NE(find_provost_move(moves, -1), moves.end());
    EXPECT_NE(find_provost_move(moves, 0), moves.end());
    EXPECT_NE(find_provost_move(moves, 1), moves.end());
    EXPECT_NE(find_provost_move(moves, 2), moves.end());
    EXPECT_NE(find_provost_move(moves, 3), moves.end());
    b.apply_move(*find_provost_move(moves, -3));
    moves = b.get_possible_moves();
    EXPECT_EQ(moves.size(), 6) << "Back TWO, forward three, stay";
    EXPECT_NE(find_provost_move(moves, -2), moves.end());
    EXPECT_NE(find_provost_move(moves, -1), moves.end());
    EXPECT_NE(find_provost_move(moves, 0), moves.end());
    EXPECT_NE(find_provost_move(moves, 1), moves.end());
    EXPECT_NE(find_provost_move(moves, 2), moves.end());
    EXPECT_NE(find_provost_move(moves, 3), moves.end());
}

TEST(activateBridge, cantMoveTooMuchInThreePlayer)
{
    caylus_board b {{1, 0, 2}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::bridge0});
    b.apply_move(place_worker {0, spot_name::bridge1});
    b.apply_move(place_worker {2, spot_name::bridge2});
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 7) << "Back three, forward three, stay";
    EXPECT_NE(find_provost_move(moves, -3), moves.end());
    EXPECT_NE(find_provost_move(moves, -2), moves.end());
    EXPECT_NE(find_provost_move(moves, -1), moves.end());
    EXPECT_NE(find_provost_move(moves, 0), moves.end());
    EXPECT_NE(find_provost_move(moves, 1), moves.end());
    EXPECT_NE(find_provost_move(moves, 2), moves.end());
    EXPECT_NE(find_provost_move(moves, 3), moves.end());
    b.apply_move(*find_provost_move(moves, -3));
    moves = b.get_possible_moves();
    EXPECT_EQ(moves.size(), 6) << "Back TWO, forward three, stay";
    EXPECT_NE(find_provost_move(moves, -2), moves.end());
    EXPECT_NE(find_provost_move(moves, -1), moves.end());
    EXPECT_NE(find_provost_move(moves, 0), moves.end());
    EXPECT_NE(find_provost_move(moves, 1), moves.end());
    EXPECT_NE(find_provost_move(moves, 2), moves.end());
    EXPECT_NE(find_provost_move(moves, 3), moves.end());
    b.apply_move(*find_provost_move(moves, -2));
    moves = b.get_possible_moves();
    EXPECT_EQ(moves.size(), 4) << "Forward three, stay";
    EXPECT_NE(find_provost_move(moves, 0), moves.end());
    EXPECT_NE(find_provost_move(moves, 1), moves.end());
    EXPECT_NE(find_provost_move(moves, 2), moves.end());
    EXPECT_NE(find_provost_move(moves, 3), moves.end());
}

TEST(activateBridge, canMoveWayUp)
{
    caylus_board b {{1, 0, 2, 3, 4}, get_test_pink_buildings()};
    b.apply_move(place_worker {1, spot_name::pink0});
    b.apply_move(place_worker {0, spot_name::bridge0});
    b.apply_move(place_worker {2, spot_name::bridge1});
    b.apply_move(place_worker {3, spot_name::bridge2});
    b.apply_move(place_worker {4, spot_name::bridge3});
    b.apply_move(place_worker {1, spot_name::bridge4});
    auto moves {b.get_possible_moves()};
    EXPECT_EQ(moves.size(), 7) << "Back three, forward three, stay";
    b.apply_move(*find_provost_move(moves, 3));
    moves = b.get_possible_moves();
    b.apply_move(*find_provost_move(moves, 3));
    moves = b.get_possible_moves();
    b.apply_move(*find_provost_move(moves, 3));
    moves = b.get_possible_moves();
    b.apply_move(*find_provost_move(moves, 3));
    moves = b.get_possible_moves();
    b.apply_move(*find_provost_move(moves, 3));
}

// Disabled until we can get further into the game so that the provost
// moves up too far.
TEST(activateBridge, DISABLED_cantMoveTooMuchInFivePlayer)
{
}
