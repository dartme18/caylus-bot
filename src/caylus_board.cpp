#include "stdafx.hpp"

#include "caylus_board.hpp"
#include "util.hpp"

enum struct phase
{
    income,
    place_workers,
    activate_builtins,
    activate_bridge,
    activate_buildings,
    activate_castle,
};

caylus_board::caylus_board(std::vector<unsigned> && player_order, std::vector<building_name> && pink)
    : move_order {std::move(player_order)}
{
    if (pink.size() != 6)
        throw std::invalid_argument {"Must start with six pink spots."};
    if (move_order.size() < 2 || move_order.size() > 5)
        throw std::invalid_argument {"This is a 2-5 player game, but " + std::to_string(move_order.size()) + " were submitted."};
    turn = move_order[0];
    buildings.push_back({spot_name::gate, get_building(building_name::gate)});
    buildings.push_back({spot_name::trading_post, get_building(building_name::trading_post)});
    buildings.push_back({spot_name::merchant_guild, get_building(building_name::merchant_guild)});
    buildings.push_back({spot_name::joust_field, get_building(building_name::joust_field)});
    buildings.push_back({spot_name::stables0, get_building(building_name::stables0)});
    buildings.push_back({spot_name::stables1, get_building(building_name::stables1)});
    buildings.push_back({spot_name::stables2, get_building(building_name::stables2)});
    buildings.push_back({spot_name::inn0, get_building(building_name::inn0)});
    buildings.push_back({spot_name::inn1, get_building(building_name::inn1)});
    buildings.push_back({spot_name::bridge0, get_building(building_name::bridge0)});
    buildings.push_back({spot_name::bridge1, get_building(building_name::bridge1)});
    buildings.push_back({spot_name::bridge2, get_building(building_name::bridge2)});
    buildings.push_back({spot_name::bridge3, get_building(building_name::bridge3)});
    buildings.push_back({spot_name::bridge4, get_building(building_name::bridge4)});
    buildings.push_back({spot_name::pink0, {pink[0]}});
    buildings.push_back({spot_name::pink1, {pink[1]}});
    buildings.push_back({spot_name::pink2, {pink[2]}});
    buildings.push_back({spot_name::pink3, {pink[3]}});
    buildings.push_back({spot_name::pink4, {pink[4]}});
    buildings.push_back({spot_name::pink5, {pink[5]}});
    buildings.push_back({spot_name::fixed_carpenter, get_building(building_name::carpenter)});
    buildings.push_back({spot_name::fixed_pedlar, get_building(building_name::pedlar)});
    buildings.push_back({spot_name::castle0, get_building(building_name::castle0)});
    buildings.push_back({spot_name::castle1, get_building(building_name::castle1)});
    buildings.push_back({spot_name::castle2, get_building(building_name::castle2)});
    buildings.push_back({spot_name::castle3, get_building(building_name::castle3)});
    buildings.push_back({spot_name::castle4, get_building(building_name::castle4)});
    for (unsigned i {0}; i < move_order.size(); ++i)
    {
        prestige.push_back(0);
        money.push_back(0);
        wood.push_back(1);
        food.push_back(2);
        stone.push_back(0);
        cloth.push_back(0);
        gold.push_back(0);
        prestige_favor_track.push_back(0);
        money_favor_track.push_back(0);
        goods_favor_track.push_back(0);
        building_favor_track.push_back(0);
        packets_submitted.push_back(0);
    }
    for (unsigned i {0}; i < move_order.size(); ++i)
    {
        switch (i)
        {
            // Starting money
        case 0: money.at(move_order.at(i)) = 5; break;
        case 1: money.at(move_order.at(i)) = 6; break;
        case 2: money.at(move_order.at(i)) = 6; break;
        case 3: money.at(move_order.at(i)) = 7; break;
        case 4: money.at(move_order.at(i)) = 7; break;
        }
    }
    income_phase();
    current_phase = phase::place_workers;
}

void caylus_board::income_phase(void)
{
    std::for_each(move_order.begin(), move_order.end(), [this](auto const & player)
        {
            auto const extra_money {std::accumulate(buildings.begin(), buildings.end(), 0u, [this, &player](auto const acc, auto const & building)
                {
                    if (static_cast<unsigned>(building.second.owner) == player)
                        return acc + building.second.generated_deniers;
                    return acc;
                })};
            money[player] += extra_money;
        });
}

std::vector<move> caylus_board::get_place_worker_moves(void) const
{
    std::vector<move> ret;
    unsigned const next_bridge_spot {std::accumulate(buildings.begin(), buildings.end(), 0u, [this](unsigned acc, auto const & b) -> unsigned
        {
            if (!is_bridge(b.second.name))
                return acc;
            return acc + ((b.second.occupied == -1) ? 0 : 1);
        })};
    spot_name const bridge_spot {get_bridge_spot(next_bridge_spot)};
    unsigned cost_of_placing {get_bridge_spot(bridge_spot) + 1};
    if (find_building(buildings, spot_name::inn1)->second.occupied == static_cast<int>(turn))
        cost_of_placing = 1;
    if (workers_placed(turn) > 5 || money.at(turn) < cost_of_placing) // workers_placed accounts for inn retention
        return {place_worker {turn, bridge_spot}};
    std::for_each(buildings.begin(), buildings.end(), [this, &ret, bridge_spot](auto const & b)
        {
            if (b.second.name == building_name::none)
                return;
            if (b.second.occupied != -1)
                return;
            if (is_bridge(b.first) && b.first != bridge_spot)
                return;
            if (move_order.size() < 3 && b.first == spot_name::stables2)
                return;
            ret.emplace_back(place_worker {turn, b.first});
        });
    return ret;
}

// When activating the gate, this function returns the moves that the user could choose
std::vector<move> caylus_board::get_gate_moves(void) const
{
    std::vector<std::pair<spot_name, building>> filtered;
    std::copy_if(buildings.begin(), buildings.end(), std::back_inserter(filtered), [this](auto const & b)
        {
            if (b.first == spot_name::inn1)
                return false;
            if (move_order.size() < 3 && b.first == spot_name::stables2)
                return false;
            if (!can_place(b.second.name))
                return false;
            if (b.second.occupied != -1)
                return false;
            if (is_bridge(b.first))
                return false;
            return true;
        });
    std::vector<move> ret;
    std::transform(filtered.begin(), filtered.end(), std::back_inserter(ret), [this](auto const & b)
        { return place_worker {turn, b.first}; });
    return ret;
}

std::vector<move> caylus_board::get_merchant_moves(void) const
{
    // For the merchant, the provost is always free to move three forward or backward.
    // (If not, fix this code!)
    return {provost_move {turn, -3}, provost_move {turn, -2}, provost_move {turn, -1}, provost_move {turn, 0},
        provost_move {turn, 1}, provost_move {turn, 2}, provost_move {turn, 3}};
}

static void add_favor_good_trade(unsigned turn, good in, std::vector<move> & moves)
{
    for (good g0 {good::wood}; g0 != good::none; ++g0)
    {
        if (in == g0) // This could be achieved at one of the other favor spots
            continue;
        for (good g1 {g0}; g1 != good::none; ++g1)
        {
            if (in == g1) // This could be achieved at one of the other favor spots
                continue;
            moves.push_back(favor_move {turn, in, g0, g1});
        }
    }
}

std::vector<move> caylus_board::get_joust_field_moves(void) const
{
    auto joust_spot {find_building(buildings, spot_name::joust_field, true)};
    int temp_player {joust_spot->second.occupied};
    if (temp_player == -1)
        return {};
    unsigned player {static_cast<unsigned>(temp_player)};
    if (!money.at(player))
    {
        ERROR("I think we should never be here. If the player doesn't have enough money to activate the joust field, "
              "the board should not be returning joust field moves.");
        return {};
    }
    if (!cloth.at(player))
    {
        ERROR("Shouldn't be here?"); // see above
        return {};
    }
    std::vector<move> ret;
    ret.push_back(favor_move {player, favor_track::prestige, std::min(prestige_favor_track.at(player) + 1, 5u)});
    ret.push_back(favor_move {player, favor_track::money, std::min(money_favor_track.at(player) + 1, 5u)});
    switch (goods_favor_track.at(player))
    {
    case 4:
        ret.push_back(favor_move {player, good::gold});
        [[fallthrough]];
    case 3:
        if (food.at(player))
            add_favor_good_trade(player, good::food, ret);
        if (wood.at(player))
            add_favor_good_trade(player, good::wood, ret);
        if (stone.at(player))
            add_favor_good_trade(player, good::stone, ret);
        if (cloth.at(player))
            add_favor_good_trade(player, good::cloth, ret);
        if (gold.at(player))
            add_favor_good_trade(player, good::gold, ret);
        [[fallthrough]];
    case 2:
        ret.push_back(favor_move {player, good::cloth});
        [[fallthrough]];
    case 1:
        ret.push_back(favor_move {player, good::stone});
        ret.push_back(favor_move {player, good::wood});
        [[fallthrough]];
    case 0:
        ret.push_back(favor_move {player, good::food});
    }
    // Building favors not handled yet
    ret.push_back(favor_move {player, building_name::none});
    return ret;
}

std::vector<move> caylus_board::get_activate_builtin_moves(void) const
{
    switch (activation_spot)
    {
    case spot_name::gate: return get_gate_moves();
    case spot_name::merchant_guild: return get_merchant_moves();
    case spot_name::joust_field: return get_joust_field_moves();
    //case spot_name::inn1: return get_ allow the player to take his worker off the inn
    default: throw std::runtime_error {"Generating moves for activation spot " + to_string(activation_spot) + " not handled."};
    }
}

std::vector<move> caylus_board::get_bridge_moves(void) const
{
    unsigned max_back {3};
    unsigned max_forward {3};
    switch (provost)
    {
    case spot_name::pink0: max_back = 0; break;
    case spot_name::pink1: max_back = 1; break;
    case spot_name::pink2: max_back = 2; break;
    case spot_name::build12: max_forward = 0; break;
    case spot_name::build11: max_forward = 1; break;
    case spot_name::build10: max_forward = 2; break;
    default: break;
    }
    auto bridge_spot {find_building(buildings, activation_spot, true)};
    auto const player {bridge_spot->second.occupied};
    if (player == -1)
        throw std::runtime_error {"Can't get bridge moves for no one"};
    max_back = std::min(money.at(player), max_back);
    max_forward = std::min(money.at(player), max_forward);
    std::vector<move> ret {provost_move {static_cast<unsigned>(player), 0}};
    for (unsigned i {0}; i < max_back; ++i)
        ret.push_back(provost_move {static_cast<unsigned>(player), -static_cast<int>(i) - 1});
    for (unsigned i {0}; i < max_forward; ++i)
        ret.push_back(provost_move {static_cast<unsigned>(player), static_cast<int>(i) + 1});
    return ret;
}

std::vector<move> caylus_board::get_collection_moves(unsigned player, std::pair<spot_name, building> const & building) const
{
    switch (building.second.name)
    {
    case building_name::pink_wood:
    case building_name::pink_stone: throw std::runtime_error {"No decision for collection building " + ::to_string(building.second.name)};
    case building_name::pink_cloth: return {pick_good {player, good::cloth}, pick_good {player, good::food}};
    case building_name::pink_food: return {pick_good {player, good::wood}, pick_good {player, good::food}};
    default: throw std::runtime_error {"Unimplemented collection building " + ::to_string(building.second.name)};
    }
}

std::vector<move> caylus_board::get_market_moves(unsigned player, std::pair<spot_name, building> const & building) const
{
    std::vector<move> moves;
    switch (building.second.name)
    {
    case building_name::pink_market:
        if (get_goods(good::cloth).at(player))
            moves.push_back(pick_good {player, good::cloth});
        if (get_goods(good::food).at(player))
            moves.push_back(pick_good {player, good::food});
        if (get_goods(good::wood).at(player))
            moves.push_back(pick_good {player, good::wood});
        if (get_goods(good::stone).at(player))
            moves.push_back(pick_good {player, good::stone});
        if (get_goods(good::gold).at(player))
            moves.push_back(pick_good {player, good::gold});
        break;
    default: throw std::runtime_error {"Unimplemented market building " + ::to_string(building.second.name)};
    }
    if (moves.size())
        moves.push_back(pick_good {player, good::none});
    return moves;
}

std::optional<move> caylus_board::get_carpenter_move(unsigned player, std::vector<good> const & required, building_name b) const
{
    if (building_count(b) < allowed_building_count(b))
        if (has_goods(player, required))
            return building_move {player, b};
    return {};
}

std::vector<move> caylus_board::get_carpenter_moves(unsigned player) const
{
    std::vector<move> moves;
    auto temp_move {get_carpenter_move(player, {good::wood, good::food}, building_name::mason)};
    if (temp_move)
        moves.push_back(std::move(*temp_move));
    temp_move = get_carpenter_move(player, {good::wood, good::food}, building_name::wood_cloth_farm);
    if (temp_move)
        moves.push_back(std::move(*temp_move));
    temp_move = get_carpenter_move(player, {good::wood, good::food}, building_name::wood_food_farm);
    if (temp_move)
        moves.push_back(std::move(*temp_move));
    temp_move = get_carpenter_move(player, {good::wood, good::stone}, building_name::lawyer);
    if (temp_move)
        moves.push_back(std::move(*temp_move));
    temp_move = get_carpenter_move(player, {good::wood, good::none}, building_name::marketplace);
    if (temp_move)
        moves.push_back(std::move(*temp_move));
    temp_move = get_carpenter_move(player, {good::wood, good::food}, building_name::lumberjack);
    if (temp_move)
        moves.push_back(std::move(*temp_move));
    temp_move = get_carpenter_move(player, {good::wood, good::none}, building_name::pedlar);
    if (temp_move)
        moves.push_back(std::move(*temp_move));
    return moves;
}

std::vector<move> caylus_board::get_activate_buildings_moves(void) const
{
    auto const b_spot {find_building(buildings, activation_spot, true)};
    if (b_spot == buildings.end())
        throw std::runtime_error {"No building at activation spot"};
    int const player {b_spot->second.occupied};
    if (player == -1)
        throw std::runtime_error {"Cannot get moves for unoccupied building."};
    switch (b_spot->second.name)
    {
    case building_name::pink_wood:
    case building_name::pink_stone:
    case building_name::pink_food:
    case building_name::pink_cloth: return get_collection_moves(player, *b_spot); break;
    case building_name::pink_market: return get_market_moves(player, *b_spot); break;
    case building_name::carpenter: return get_carpenter_moves(player); break;
    default: throw std::runtime_error {"Unimplemented moves for " + ::to_string(b_spot->second.name)};
    }
}

bool caylus_board::can_create_packet(unsigned player) const
{
    unsigned count {food.at(player) ? 1u : 0u};
    if (!count)
        return false;
    count += stone.at(player) ? 1 : 0;
    count += wood.at(player) ? 1 : 0;
    if (count > 2)
        return true;
    count += cloth.at(player) ? 1 : 0;
    if (count > 2)
        return true;
    count += gold.at(player) ? 1 : 0;
    return count > 2;
}

std::vector<std::vector<packet>> caylus_board::get_possible_packets(unsigned player) const
{
    if (!food.at(player))
        return {};
    std::vector<good> other_goods;
    other_goods.insert(other_goods.end(), stone.at(player), good::stone);
    other_goods.insert(other_goods.end(), cloth.at(player), good::cloth);
    other_goods.insert(other_goods.end(), wood.at(player), good::wood);
    other_goods.insert(other_goods.end(), gold.at(player), good::gold);
    return ::get_possible_packets(food.at(player), std::move(other_goods));
}

std::vector<move> caylus_board::get_castle_moves(void) const
{
    auto const b_spot {find_building(buildings, activation_spot, true)};
    if (b_spot == buildings.end())
        throw std::runtime_error {"No castle spot at activation spot"};
    int const player {b_spot->second.occupied};
    if (player == -1)
        throw std::runtime_error {"Cannot get moves for unoccupied castle space."};
    if (!food.at(player))
        throw std::runtime_error {"Should not be offering castle choices to player who can't build packet."};
    std::vector<move> ret;
    for (auto & packet : get_possible_packets(player))
        ret.push_back(castle_packet {static_cast<unsigned>(player), std::move(packet)});
    return ret;
}

std::vector<move> caylus_board::get_possible_moves(void) const
{
    switch (current_phase)
    {
    case phase::place_workers: return get_place_worker_moves();
    case phase::activate_builtins: return get_activate_builtin_moves();
    case phase::activate_bridge: return get_bridge_moves();
    case phase::activate_buildings: return get_activate_buildings_moves();
    case phase::activate_castle: return get_castle_moves();
    default: throw std::runtime_error {"current phase not handled."};
    }
    return {};
}

void caylus_board::activate_gate(move const & m)
{
    auto gate_spot {find_building(buildings, spot_name::gate, true)};
    if (gate_spot->second.occupied != -1)
    {
        if (std::holds_alternative<no_move>(m))
            return;
        if (!std::holds_alternative<gate>(m))
            throw std::invalid_argument {"gate move required to activate the gate"};
        gate const & real_move {std::get<gate>(m)};
        if (static_cast<int>(real_move.player) != gate_spot->second.occupied)
            throw std::invalid_argument {"Wrong player on gate move"};
        auto destination_spot {find_building(buildings, real_move.destination)};
        if (destination_spot == buildings.end())
            throw std::invalid_argument {"The gate destination, " + to_string(real_move.destination) + " can't be found."};
        if (destination_spot->second.occupied != -1)
            throw std::invalid_argument {"The gate destination is already occupied."};
        if (!can_place(destination_spot->second.name))
            throw std::invalid_argument {"Cannot place on gate destination, " + to_string(real_move.destination)};
        destination_spot->second.occupied = real_move.player;
        INFO("{} moves worker from gate to {}.", real_move.player, to_string(real_move.destination));
    }
    activation_spot = spot_name::trading_post;
    continue_activate_builtins({});
}

void caylus_board::activate_trading_post(move const &)
{
    auto trading_post_spot {find_building(buildings, spot_name::trading_post)};
    if (trading_post_spot->second.occupied != -1)
        money.at(trading_post_spot->second.occupied) += 3;
    activation_spot = spot_name::merchant_guild;
    continue_activate_builtins({});
}

void caylus_board::move_provost(provost_move const & m)
{
    bool minus_three = true;
    bool minus_two = true;
    bool minus_one = true;
    bool plus_three = true;
    bool plus_two = true;
    bool plus_one = true;
    switch (provost)
    {
    case spot_name::pink0: minus_one = minus_two = minus_three = false; break;
    case spot_name::pink1: minus_two = minus_three = false; break;
    case spot_name::pink2: minus_three = false; break;
    default: break;
    }
    if (m.move_count == -3 && !minus_three)
        throw std::invalid_argument {"Cannot move the provost -3"};
    if (m.move_count == -2 && !minus_two)
        throw std::invalid_argument {"Cannot move the provost -2"};
    if (m.move_count == -1 && !minus_one)
        throw std::invalid_argument {"Connet move the provost back"};
    if (m.move_count == 1 && !plus_one)
        throw std::invalid_argument {"Connet move the provost forward"};
    if (m.move_count == 2 && !plus_two)
        throw std::invalid_argument {"Connet move the provost forward two"};
    if (m.move_count == 3 && !plus_three)
        throw std::invalid_argument {"Connet move the provost forward three"};
    int spots {m.move_count};
    while (spots)
        if (spots > 0)
        {
            ++provost;
            --spots;
        }
        else
        {
            --provost;
            ++spots;
        }
}

void caylus_board::activate_merchant_guild(move const & m)
{
    auto merchant_guild_spot {find_building(buildings, spot_name::merchant_guild, true)};
    if (merchant_guild_spot->second.occupied != -1)
    {
        if (std::holds_alternative<no_move>(m))
            return;
        if (!std::holds_alternative<provost_move>(m))
            throw std::invalid_argument {"provost move required"};
        auto real_move {std::get<provost_move>(m)};
        if (real_move.player != turn)
            throw std::invalid_argument {"Wrong player activating merchant guild"};
        if (real_move.move_count > 3 || real_move.move_count < -3)
            throw std::invalid_argument {"Provost can only move [-3,3]"};
        move_provost(real_move);
    }
    activation_spot = spot_name::joust_field;
    continue_activate_builtins({});
}

void caylus_board::transfer_good_to(unsigned player, good g, int amount)
{
    auto & goods {get_real_goods(g)};
    if (static_cast<int>(goods.at(player)) + amount < 0)
        throw std::invalid_argument {"Player " + std::to_string(player) + " has " + std::to_string(goods.at(player)) + " " + ::to_string(g) + " which is not enough to give " + std::to_string(-amount) + "."};
    goods.at(player) += amount;
}

void caylus_board::trade_for_favor(favor_move const & m)
{
    transfer_good_to(turn, m.giving, -1);
    transfer_good_to(turn, m.receiving0);
    transfer_good_to(turn, m.receiving1);
}

void caylus_board::get_favor(favor_move const & m)
{
    DEBUG("Getting favor with {}", m.to_string());
    {
        std::vector<unsigned> * track {nullptr};
        switch (m.track)
        {
        case favor_track::prestige: track = &prestige_favor_track; break;
        case favor_track::money: track = &money_favor_track; break;
        case favor_track::goods: track = &goods_favor_track; break;
        case favor_track::building: track = &building_favor_track; break;
        }
        if (track->at(turn) < 5)
            ++track->at(turn);
    }
    switch (m.track)
    {
    case favor_track::prestige: prestige.at(turn) += prestige_favor_track.at(turn); break;
    case favor_track::money: money.at(turn) += money_favor_track.at(turn) + 2; break;
    case favor_track::goods:
        switch (m.column)
        {
        case 1: ++food.at(turn); break;
        case 2:
            if (goods_favor_track.at(turn) < 2)
                throw std::invalid_argument {"Not eligible for wood favors yet."};
            ++wood.at(turn);
            break;
        case 3:
            if (goods_favor_track.at(turn) < 2)
                throw std::invalid_argument {"Not eligible for stone favors yet."};
            ++stone.at(turn);
            break;
        case 4:
            if (goods_favor_track.at(turn) < 3)
                throw std::invalid_argument {"Not eligible for cloth favors yet."};
            ++cloth.at(turn);
            break;
        case 5:
            if (goods_favor_track.at(turn) < 4)
                throw std::invalid_argument {"Not eligible to trade goods for favors yet."};
            trade_for_favor(m);
            break;
        case 6:
            ++gold.at(turn);
            break;
            if (goods_favor_track.at(turn) < 5)
                throw std::invalid_argument {"Not eligible for gold favors yet."};
            ++gold.at(turn);
            break;
        }
        break;
    case favor_track::building:
        switch (get_building_type(m.building))
        {
        case building_type::wood:
            if (building_favor_track.at(turn) < 2)
                throw std::invalid_argument {"Not eligible to build wood buildings as a favor."};
            // If a favor was earned, don't pop the latest favor
            build(m.building, good::wood);
            break;
        case building_type::stone:
            if (building_favor_track.at(turn) < 3)
                throw std::invalid_argument {"Not eligible to build stone buildings as a favor."};
            build(m.building, good::stone);
            break;
        case building_type::green:
            if (building_favor_track.at(turn) < 4)
                throw std::invalid_argument {"Not eligible to build green buildings as a favor."};
            build(m.building, good::gold);
            break;
        case building_type::blue:
            if (building_favor_track.at(turn) < 5)
                throw std::invalid_argument {"Not eligible to build blue buildings as a favor."};
            build(m.building);
            break;
        default:
            throw std::invalid_argument {"Cannot build " + ::to_string(get_building_type(m.building)) + " as a favor."};
        }
        break;
    }
}

void caylus_board::activate_joust_field(move const & m)
{
    auto jousting_spot {find_building(buildings, spot_name::joust_field, true)};
    if (jousting_spot->second.occupied != -1)
    {
        if (!cloth.at(turn) || !money.at(turn))
        {
            ++activation_spot;
            continue_activate_builtins({});
        }
        if (std::holds_alternative<no_move>(m))
            return;
        if (!std::holds_alternative<favor_move>(m))
            throw std::invalid_argument {"Jousting field requires favor move"};
        auto real_move {std::get<favor_move>(m)};
        if (real_move.player != turn)
            throw std::invalid_argument {"Wrong player getting favor for Jousting Field"};
        --cloth.at(turn);
        --money.at(turn);
        get_favor(real_move);
    }
    ++activation_spot;
    continue_activate_builtins({});
}

void caylus_board::activate_stable(move const &)
{
    std::vector<unsigned> moving_players;
    auto stable0_spot {find_building(buildings, spot_name::stables0, true)};
    if (stable0_spot->second.occupied != -1)
    {
        move_order.erase(std::find(move_order.begin(), move_order.end(), stable0_spot->second.occupied));
        move_order.insert(move_order.begin(), stable0_spot->second.occupied);
    }
    ++activation_spot;
    auto stable1_spot {find_building(buildings, spot_name::stables1, true)};
    if (stable1_spot->second.occupied != -1)
    {
        move_order.erase(std::find(move_order.begin(), move_order.end(), stable1_spot->second.occupied));
        move_order.insert(move_order.begin() + 1, stable1_spot->second.occupied);
    }
    ++activation_spot;
    auto stable2_spot {find_building(buildings, spot_name::stables2, true)};
    if (stable2_spot->second.occupied != -1)
    {
        if (move_order.size() < 3)
        {
            ERROR("Cannot use third stable spot with less than three players");
        }
        else
        {
            move_order.erase(std::find(move_order.begin(), move_order.end(), stable2_spot->second.occupied));
            move_order.insert(move_order.begin() + 2, stable2_spot->second.occupied);
        }
    }
    ++activation_spot;
    continue_activate_builtins({});
}

void caylus_board::activate_inn(move const &)
{
    auto inn1_spot {find_building(buildings, spot_name::inn1, true)};
    if (inn1_spot->second.occupied != -1)
    {
        auto inn0_spot {find_building(buildings, spot_name::inn0, true)};
        inn0_spot->second.occupied = inn1_spot->second.occupied;
    }
    next_phase();
}

void caylus_board::continue_activate_builtins(move const & m)
{
    switch (activation_spot)
    {
    case spot_name::gate: return activate_gate(m);
    case spot_name::trading_post: return activate_trading_post(m);
    case spot_name::merchant_guild: return activate_merchant_guild(m);
    case spot_name::joust_field: return activate_joust_field(m);
    case spot_name::stables0:
    case spot_name::stables1:
    case spot_name::stables2: return activate_stable(m);
    case spot_name::inn0:
    case spot_name::inn1: return activate_inn(m);
    default: throw std::runtime_error {to_string(activation_spot) + " activate not implemented"};
    }
}

void caylus_board::continue_activate_bridge(move const & m)
{
    auto bridge_spot {find_building(buildings, activation_spot, true)};
    auto const player {bridge_spot->second.occupied};
    if (player == -1)
        return next_phase();
    if (!std::holds_alternative<provost_move>(m))
        return;
    provost_move real_move {std::get<provost_move>(m)};
    if (money.at(player) < std::abs(real_move.move_count))
        throw std::invalid_argument {"Not enough coins to move provost at bridge"};
    if (static_cast<int>(real_move.player) != player)
        throw std::invalid_argument {"Can't move the bridge for someone else"};
    money.at(player) -= std::abs(real_move.move_count);
    move_provost(real_move);
    ++activation_spot;
    continue_activate_bridge({});
}

void caylus_board::transfer_good(unsigned player, good g, int count)
{
    switch (g)
    {
    case good::food: food.at(player) += count; break;
    default: throw std::invalid_argument {"not covered"};
    }
}

void caylus_board::build_wood_building(building_move const & m)
{
    spot_name dest {get_new_building_destination()};
    if (dest == spot_name::none)
        throw std::invalid_argument {"No place to build wood building"};
    prestige.at(m.player) += prestige_from_build(m.to_build);
    building building {get_building(m.to_build)};
    building.owner = m.player;
    building.occupied = -1;
    buildings.emplace_back(dest, std::move(building));
}

// Returns false if the player needs to make a decision
bool caylus_board::activate_carpenter(unsigned player, move const & m)
{
    if (!std::holds_alternative<building_move>(m))
        return false;
    building_move real_move {std::get<building_move>(m)};
    if (real_move.player != player)
        throw std::invalid_argument {"Cannot build a building for another player"};
    if (real_move.to_build == building_name::none)
        return true;
    if (get_building_type(real_move.to_build) != building_type::wood)
        throw std::invalid_argument {"Can only build wood buildings on the carpenter"};
    auto const count_existing {building_count(real_move.to_build)};
    if (count_existing >= allowed_building_count(real_move.to_build))
        throw std::invalid_argument {"Cannot build a building that is already built."};
    build_wood_building(real_move);
    return true;
}

// Returns false if the player needs to make a decision
bool caylus_board::activate_market(unsigned player, move const & m, unsigned /*deniers*/, unsigned /*goods*/)
{
    if (!std::holds_alternative<pick_good>(m))
        return false;
    pick_good real_move {std::get<pick_good>(m)};
    auto & the_good {get_real_goods(real_move.picked)};
    if (the_good.at(player))
        --the_good.at(player);
    else
        throw std::invalid_argument {"No food to trade"};
    money.at(player) += 4;
    return true;
}

bool caylus_board::transfer_picked_good(unsigned player, move const & m, std::vector<good> const & possibilities)
{
    if (!std::holds_alternative<pick_good>(m))
        return false;
    pick_good real_move {std::get<pick_good>(m)};
    if (real_move.picked == good::none)
    {
        WARN("No free good picked!?");
        return true;
    }
    if (std::find(possibilities.begin(), possibilities.end(), real_move.picked) == possibilities.end())
        throw std::invalid_argument {"Not an acceptable choice of good."};
    ++get_real_goods(real_move.picked).at(player);
    return true;
}

// Returns false if the player needs to make a decision
bool caylus_board::activate_collection(unsigned player, std::pair<spot_name, building> const & building, move const & m)
{
    switch (building.second.name)
    {
    case building_name::pink_wood: transfer_good_to(player, good::wood); return true;
    case building_name::pink_stone: transfer_good_to(player, good::stone); return true;
    case building_name::pink_cloth: return transfer_picked_good(player, m, {good::cloth, good::food});
    case building_name::pink_food: return transfer_picked_good(player, m, {good::wood, good::food});
    default: throw std::invalid_argument {"Invalid building collection activation"};
    }
}

bool caylus_board::activate_pedlar(unsigned player, move const & m, unsigned /*cost*/, unsigned /*goods_received*/)
{
    if (!std::holds_alternative<pick_good>(m))
        return false;
    pick_good real_move {std::get<pick_good>(m)};
    if (real_move.player != player)
        throw std::invalid_argument {"Can't peddle good for another player."};
    if (real_move.picked == good::none)
        return true;
    if (money.at(player) < 2)
        throw std::invalid_argument {"Not enough money for pedlar."};
    money.at(player) -= 2;
    transfer_good(player, real_move.picked, 1);
    return true;
}

void caylus_board::continue_activate_buildings(move const & m)
{
    auto const b_spot {find_building(buildings, activation_spot)};
    if (b_spot != buildings.end())
    {
        int const player {b_spot->second.occupied};
        if (player != -1)
        {
            // We're not concerned now about no_move. If no_move is submitted, we may
            // still be able to make a good deal of progress.
            // This only makes sure that one player isn't submitting a move for another.
            if (!std::holds_alternative<no_move>(m) && static_cast<int>(get_player(m)) != player)
                throw std::invalid_argument {"Can't activate building for another player"};
            switch (b_spot->second.name)
            {
            case building_name::pink_wood:
            case building_name::pink_stone:
            case building_name::pink_food:
            case building_name::pink_cloth:
                if (!activate_collection(player, *b_spot, m))
                    return;
                break;
            case building_name::pink_market:
                if (!activate_market(player, m, 4, 1))
                    return;
                break;
            case building_name::carpenter:
                if (!activate_carpenter(player, m))
                    return;
                break;
            case building_name::fixed_pedlar:
                if (!activate_pedlar(player, m, 2, 1))
                    return;
                break;
            default: std::runtime_error {"unhandled second spot"};
            }
        }
    }
    ++activation_spot;
    if (activation_spot == spot_name::castle0)
        next_phase();
    else
        continue_activate_buildings({});
}

// If true is returned, continue activation of castle.
bool caylus_board::activate_one_castle(move const & m)
{
    auto const spot {find_building(buildings, activation_spot)};
    if (spot == buildings.end())
        throw std::runtime_error {"Spot should be a castle spot."};
    int const player {spot->second.occupied};
    if (player == -1)
    {
        activation_spot = spot_name::none;
        return false;
    }
    if (std::holds_alternative<no_move>(m))
        return false;
    castle_packet real_move {std::get<castle_packet>(m)};
    if (!real_move.picked.size())
    { // Not contributing a packet
        prestige.at(player) -= std::min(2u, prestige.at(player));
    }
    //if (!has_goods(static_cast<unsigned>(player), real_move.picked))
    //throw std::invalid_argument{"Player doesn't have goods for castle"};
    //if (real_move.picked.size() % 3)
    //throw std::invalid_argument{"Must send a multiple of three packets"};
    //std::for_each(real_move.picked.begin(), real_move.picked.end(), [](std::vector<good> const & goods)
    //{
    //if (std::find(goods.begin(), goods.end(), good::food) == goods.end())
    //throw std::invalid_argument{"Must send one food with each castle packet"};
    //
    //});
    //std::sort(real_move.picked.begin(), real_move.picked.end());
    // For each submitted packet
    //for (unsigned spot{0}; spot < real_move.picked.size(); spot+= 3)
    //{
    //unsigned first{spot};
    //unsigned second{spot+1};
    //if (real_move.picked.at(spot) == good::food)
    //{
    //++first;
    //second = first+1;
    //}
    //else if (real_move.picked.at(spot+1) == good::food)
    //second = first+2;
    //else if (real_move.picked.at(spot+2) != good::food)
    //throw std::invalid_argument{"Each packet must have food"};
    //if (real_move.picked.at(first) == real_move.picked.at(second) ||
    //real_move.picked.at(first) == good::food ||
    //real_move.picked.at(second) == good::food)
    //throw std::invalid_argument{"Each packet must have three unique goods"};
    //}
    //for (good temp_g : real_move.picked)
    //transfer_good_to(player, temp_g, -1);
    packets_submitted.at(player) += real_move.picked.size();
    ++activation_spot;
    return true;
}

void caylus_board::continue_activate_castle(move const & m)
{
    while (activation_spot != spot_name::none)
    {
        if (!activate_one_castle(m))
            break;
    }
}

void caylus_board::start_activate_builtins_phase(void)
{
    current_phase = phase::activate_builtins;
    activation_spot = spot_name::gate;
    continue_activate_builtins({});
}

void caylus_board::start_activate_bridge_phase(void)
{
    current_phase = phase::activate_bridge;
    activation_spot = spot_name::bridge0;
    continue_activate_bridge({});
}

void caylus_board::start_activate_buildings_phase(void)
{
    current_phase = phase::activate_buildings;
    activation_spot = spot_name::pink0;
    continue_activate_buildings({});
}

void caylus_board::start_activate_castle_phase(void)
{
    current_phase = phase::activate_castle;
    activation_spot = spot_name::castle0;
    for (unsigned spot {0}; spot < move_order.size(); ++spot)
        packets_submitted.at(spot) = 0;
    continue_activate_castle({});
}

void caylus_board::next_phase(void)
{
    switch (current_phase)
    {
    case phase::place_workers: start_activate_builtins_phase(); break;
    case phase::activate_builtins: start_activate_bridge_phase(); break;
    case phase::activate_bridge: start_activate_buildings_phase(); break;
    case phase::activate_buildings: start_activate_castle_phase(); break;
    default: throw std::runtime_error {"not implement yet phase"};
    }
}

unsigned caylus_board::workers_placed(unsigned player) const
{
    // This should also take account of a player being on inn0
    return static_cast<unsigned>(std::count_if(buildings.begin(), buildings.end(), [player](auto const & s)
        { return s.second.occupied == static_cast<int>(player); }));
}

void caylus_board::place_worker_next_turn(void)
{
    for (unsigned i {0}; i < move_order.size(); ++i)
    {
        auto const & turn_spot {std::find(move_order.begin(), move_order.end(), turn)};
        if (turn_spot == move_order.end())
            throw std::runtime_error {"BUG: Couldn't find current player in move order. This is bad."};
        if (turn_spot + 1 == move_order.end())
            turn = move_order[0];
        else
            turn = *(turn_spot + 1);
        spot_name bridge_spot {spot_name::bridge0};
        bool on_bridge {false};
        do
        {
            auto b {find_building(buildings, bridge_spot)};
            if (b->second.occupied == static_cast<int>(turn))
            {
                on_bridge = true;
                break;
            }
            else if (b->second.occupied == -1)
                // A small optimization. If we're at an unmanned bridge spot, we can
                // trust that the rest of the bridge spots are unmanned, too.
                break;
        } while (++bridge_spot <= spot_name::bridge4);
        if (!on_bridge)
            return;
    }
    throw std::runtime_error {"Could not get next turn."};
}

void caylus_board::place_a_worker(move const & m)
{
    if (!std::holds_alternative<place_worker>(m))
        throw std::invalid_argument {"Must pass a place_worker during place_workers phase"};
    place_worker const & real_move {std::get<place_worker>(m)};
    if (real_move.player != turn)
        throw std::invalid_argument {"It is " + std::to_string(turn) + "'s turn, but " + std::to_string(real_move.player) + " was submitted."};
    if (real_move.spot == spot_name::inn1)
        throw std::invalid_argument {"You may not place on inn1. Inn1 is only accessible from activating inn0."};
    auto building_spot {find_building(buildings, real_move.spot, true)};
    if (!can_place(building_spot->second.name))
        throw std::invalid_argument {to_string(real_move.spot) + " cannot be placed upon."};
    if (building_spot->second.occupied != -1)
        throw std::invalid_argument {to_string(real_move.spot) + " already taken."};
    if (!is_bridge(building_spot->first) && workers_placed(real_move.player) > 5)
        throw std::invalid_argument {"Only six workers per player."};
    if (building_spot->first == spot_name::stables2 && move_order.size() < 3)
        throw std::invalid_argument {"Cannot place on stable-3 in a two-player game."};
    if (!is_bridge(building_spot->first))
    {
        auto const taken_bridge_spots {std::count_if(buildings.begin(), buildings.end(), [this](auto const & b)
            { return is_bridge(b.first) && b.second.occupied != -1; })};
        auto cost_of_spot {taken_bridge_spots + 1};
        if (find_building(buildings, spot_name::inn1)->second.occupied == static_cast<int>(turn))
            cost_of_spot = 1;
        if (money.at(turn) < cost_of_spot)
            throw std::invalid_argument {"Not enough money to place"};
        money.at(turn) -= taken_bridge_spots + 1;
    }
    building_spot->second.occupied = real_move.player;
    if (is_bridge(building_spot->first))
    {
        if (building_spot->first == spot_name::bridge0)
            // First passer gets a denier
            ++money[turn];
        if (get_bridge_spot(building_spot->first) + 1 == move_order.size())
            // The bridge is full.
            return next_phase();
    }
    place_worker_next_turn();
}

void caylus_board::apply_move(move const & m)
{
    switch (current_phase)
    {
    case phase::place_workers: return place_a_worker(m);
    case phase::activate_builtins: return continue_activate_builtins(m);
    case phase::activate_bridge: return continue_activate_bridge(m);
    case phase::activate_buildings: return continue_activate_buildings(m);
    default: throw std::runtime_error {"unhandled phase"};
    }
}

std::vector<unsigned> const & caylus_board::get_money(void) const
{
    return money;
}

// Returns true if a favor was gained as a result of building this building
bool caylus_board::build(building_name, good)
{
    INFO("Not implemented building yet.");
    return false;
}

std::vector<unsigned> const & caylus_board::get_move_order(void) const
{
    return move_order;
}

std::vector<unsigned> const & caylus_board::get_goods(good g) const
{
    return const_cast<caylus_board *>(this)->get_real_goods(g);
}

std::vector<unsigned> & caylus_board::get_real_goods(good g)
{
    switch (g)
    {
    case good::food: return food;
    case good::stone: return stone;
    case good::cloth: return cloth;
    case good::gold: return gold;
    case good::wood: return wood;
    default: throw new std::invalid_argument {"No good to return"};
    }
}

unsigned caylus_board::building_count(building_name the_b) const
{
    return std::count_if(buildings.begin(), buildings.end(), [the_b](auto const & b)
        { return b.second.name == the_b; });
}

spot_name caylus_board::get_new_building_destination(void) const
{
    spot_name building_spot {spot_name::build0};
    do
    {
        if (building_spot == spot_name::fixed_gold)
            continue;
        auto building {find_building(buildings, building_spot, false)};
        if (building == buildings.end())
            return building_spot;
    } while (building_spot != spot_name::none);
    return building_spot;
}

std::vector<std::pair<spot_name, building>> const & caylus_board::get_buildings(void) const
{
    return buildings;
}

std::vector<unsigned> const & caylus_board::get_prestige(void) const
{
    return prestige;
}

// In the vector of goods, "none" means "any".
bool caylus_board::has_goods(unsigned player, std::vector<good> const & g) const
{
    const unsigned wood_required {static_cast<unsigned>(std::count(g.begin(), g.end(), good::wood))};
    if (wood.at(player) < wood_required)
        return false;
    const unsigned food_required {static_cast<unsigned>(std::count(g.begin(), g.end(), good::food))};
    if (food.at(player) < food_required)
        return false;
    const unsigned stone_required {static_cast<unsigned>(std::count(g.begin(), g.end(), good::stone))};
    if (stone.at(player) < stone_required)
        return false;
    const unsigned cloth_required {static_cast<unsigned>(std::count(g.begin(), g.end(), good::cloth))};
    if (cloth.at(player) < cloth_required)
        return false;
    const unsigned gold_required {static_cast<unsigned>(std::count(g.begin(), g.end(), good::gold))};
    if (gold.at(player) < gold_required)
        return false;
    unsigned any_required {static_cast<unsigned>(std::count(g.begin(), g.end(), good::none))};
    if (any_required)
    {
        unsigned extra_wood = wood.at(player) - wood_required;
        unsigned extra_stone = stone.at(player) - stone_required;
        unsigned extra_gold = gold.at(player) - gold_required;
        unsigned extra_cloth = cloth.at(player) - cloth_required;
        unsigned extra_food = food.at(player) - food_required;
        return extra_wood + extra_stone + extra_gold + extra_cloth + extra_food >= any_required;
    }
    return true;
}
